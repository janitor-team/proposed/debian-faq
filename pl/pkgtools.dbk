<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="pkgtools"><title>Narzędzia do zarządzania pakietami w systemie Debian</title>
<section id="pkgprogs"><title>Jakie programy do zarządzania swoimi pakietami udostępnia?</title>
<section id="dpkg"><title>dpkg</title>
<para>
Jest to główny program służący do zarządzania pakietami.
<command>dpkg</command> może być wywołany z wieloma różnymi opcjami.
Najczęściej stosowane to:
</para>
<itemizedlist>
<listitem>
<para>
<literal>dpkg --help</literal> przedstawia dostępne opcje.
</para>
</listitem>
<listitem>
<para>
<literal>dpkg --info foo_VVV-RRR.deb</literal> wyświetla informacje o wybranym
pakiecie.
</para>
</listitem>
<listitem>
<para>
<literal>dpkg --install foo_VVV-RRR.deb</literal> instaluje (rozpakowuje i
konfiguruje) pakiet na dysku twardym.
</para>
</listitem>
<listitem>
<para>
<literal>dpkg --unpack foo_VVV-RRR.deb</literal> rozpakowuje, lecz pozostawia
nieskonfigurowany pakiet.  Zauważ, że nieskonfigurowany pakiet może okazać
się bezużyteczny.  Niektóre pliki mogą wymagać dalszej obróbki i
dostosowania do Twoich potrzeb, aby pakiet/program działał poprawnie.
Polecenie to pozostawia wszelkie zainstalowane dotychczas wersje programów i
wykonuje polecenia przedinstalacyjne (zobacz <xref linkend="maintscripts"/>),
zawarte w pakiecie.
</para>
</listitem>
<listitem>
<para>
Konfiguracja pakietu, który został wcześniej rozpakowany: <literal>dpkg
--configure foo</literal>.  To polecenie uruchamia, między innymi,
poinstalacyjne skrypty (zobacz: <xref linkend="maintscripts"/>), powiązane z
danym pakietem.  Uaktualnia również pliki wymienione w <literal>pliku
konfiguracyjnym</literal> danego pakietu.  Zauważ, że operacja ,,configure''
wymaga jako argumentu nazwy pakietu (np.  foo), a <emphasis>nie</emphasis>
nazwy pliku z pakietem (np.  foo_VVV-RRR.deb).
</para>
</listitem>
<listitem>
<para>
Rozpakowanie pojedynczego pliku nazwanego ,,blurf'' (lub grupy plików
nazwanych ,,blurf*'') z pakietu Debiana: <literal>dpkg --fsys-tarfile
foo_VVV-RRR.deb | tar -xf - blurf*</literal>
</para>
</listitem>
<listitem>
<para>
Usunięcie pakietu (bez jego plików konfiguracyjnych): <literal>dpkg --remove
foo</literal>.
</para>
</listitem>
<listitem>
<para>
Usunięcie pakietu (razem z jego plikami konfiguracyjnymi): <literal>dpkg
--purge foo</literal>.
</para>
</listitem>
<listitem>
<para>
Wyświetlenie stanu instalacji pakietów zawierających ciąg znaków (lub wg
wyrażenia regularnego) ,,foo*'': <literal>dpkg --list 'foo*'</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="dselect"><title>dselect</title>
<para>
Ten program zapewnia Debianowi interfejs dla systemu zarządzania pakietami z
opcjami pogrupowanymi w menu.  Jest on szczególnie przydatny przy instalowaniu
i uaktualnianiu systemu na dużą skalę.
</para>
<para>
<command>dselect</command> potrafi:
</para>
<itemizedlist>
<listitem>
<para>
prowadzić/wspomagać użytkownika w trakcie, gdy ten wybiera pakiety do
instalacji lub usunięcia, wykluczając jednocześnie możliwość powstania
konfliktów pomiędzy zainstalowanymi pakietami i zapewniając właściwą
pracę zainstalowanych programów poprzez instalację wszystkich wymaganych
przez nie pakietów.
</para>
</listitem>
<listitem>
<para>
ostrzegać użytkownika o niespójności lub niekompatybilności pomiędzy
pakietami, które wybrał do zainstalowania;
</para>
</listitem>
<listitem>
<para>
ustalać w jakiej kolejności pakiety powinny zostać zainstalowane;
</para>
</listitem>
<listitem>
<para>
automatycznie przeprowadzać proces instalacji lub usunięcia, oraz
</para>
</listitem>
<listitem>
<para>
prowadzić użytkownika przez proces wymaganej dla poszczególnych pakietów
konfiguracji.
</para>
</listitem>
</itemizedlist>
<para>
<command>dselect</command> przedstawia użytkownikowi na początek menu
złożone z 7 opcji/elementów, z których każdy/a reprezentuje inny aspekt
funkcjonalności programu.  Użytkownik może wybrać jedną z opcji używając
klawiszy strzałek aby podświetlić wybraną opcję i zatwierdzić wybór
naciskając klawisz/przycisk <emphasis>&lt;enter&gt;</emphasis>.
</para>
<para>
Dalsze zachowanie programu zależy od dokonanego wcześniej wyboru.  Jeżeli
nie wybrano <literal>Dostęp/Access</literal> ani
<literal>Wybór/Select</literal>, wtedy <command>dselect</command> po prostu
wykona wybrane polecenie: np.  jeżeli użytkownik wybrał polecenie
<literal>Usuwanie/Remove</literal> wtedy dselect przystąpi do usuwania
wszystkich plików wybranych/zaznaczonych ostatnio do usunięcia przy pomocy
polecenia <literal>Wybór/Select</literal>.
</para>
<para>
Zarówno opcja <literal>Dostęp/Access</literal>, jak i opcja
<literal>Wybór/Select</literal>, prowadzą do dodatkowego menu wyboru.  W
obydwu przypadkach, każde menu zostaje wyświetlone osobno, na podzielonym
ekranie; w górnej części ekranu znajdują się opcje wyboru, a w dolnej
części krótkie opisy ("info") dla każdej z wybranych opcji.
</para>
<para>
Dostępna jest także poszerzona/rozszerzona dokumentacja online, użyj
klawisza '?'  aby uzyskać dostęp do pomocy w dowolnej chwili.
</para>
<para>
Kolejność, w jakiej czynności są prezentowane w pierwszym menu programu
<command>dselect</command>, reprezentuje kolejność w jakiej użytkownik
normalnie dokonuje instalacji pakietów.  Jednakże można wybrać dowolną
pozycję z menu, tak często, jak to konieczne.
</para>
<itemizedlist>
<listitem>
<para>
Rozpoczynamy od wybrania <emphasis role="strong">sposobu dostępu</emphasis>.
(ang.  ,,Access Method'').  Jest to czynność, dzięki której można
zaplanować dostęp do źródła pakietów Debiana.  Przykładowo, niektórzy
mają pakiety na płytach CD, a inni pobierają je z serwera FTP.  Wybrany
sposób jest zapamiętywany także po wyjściu z programu, więc jeśli nie
zmieniamy źródła od poprzedniego razu, nie jest konieczne ponowne
wywoływanie tej opcji.
</para>
</listitem>
<listitem>
<para>
Teraz <emphasis role="strong">uaktualnij</emphasis> (Update) listę dostępnych
pakietów.  Aby tego dokonać, <command>dselect</command> odczytuje plik
,,Packages.gz'', który powinien znajdować się w głównym katalogu
zawierającym przeznaczone do instalacji pakiety Debiana.  (Jeżeli brak jest
takiego pliku, <command>dselect</command> utworzy go dla Ciebie.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Wybór</emphasis> (Select) określa pakiety do
zainstalowania w tym systemie.  Po wybraniu tej pozycji menu, użytkownik jest
najpierw zapoznawany z pełnym zestawem pomocy (chyba że zastosowano opcję
`--expert' w linii poleceń).  Gdy opuszczamy ekran z pomocą, pojawia się
ekran umożliwiający wybór pakietów do instalacji lub usunięcia.
</para>
<para>
Górna część ekranu zawiera przewijaną listę pakietów poukładanych w
grupy związane ze sobą.  Dolna część zawiera opis pakietu lub grupy
pakietów aktualnie podświetlonych.
</para>
<para>
Podświetlając nazwę pakietu lub grupę pakietów, wskazujemy stan, który
mamy zamiar zmieniać.  Następnie możesz zaznaczyć pakiet:
</para>
<variablelist>
<varlistentry>
<term>do instalacji:</term>
<listitem>
<para>
Dokonuje się tego przy pomocy klawisza `+'.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>do usunięcia:</term>
<listitem>
<para>
Pakiety mogą być usuwane dwoma sposobami:
</para>
<itemizedlist>
<listitem>
<para>
usunięcie (remove): usuwa większość plików związanych z tym pakietem.
Pozostawia jednak pliki, określone jako konfiguracyjne (zobacz: <xref
linkend="conffile"/>) oraz konfigurację pakietu Dokonuje się tego klawiszem
`-'.
</para>
</listitem>
<listitem>
<para>
oczyszczenie (purge): usuwa <emphasis>wszystkie</emphasis> pliki wchodzące w
skład tego pakietu.  Służy do tego klawisz `_'.
</para>
</listitem>
</itemizedlist>
<para>
Zauważ, że nie jest możliwe usunięcie grupy ,,Wszystkie pakiety'' ("All
Packages").  Gdy tego spróbujesz, system zostanie zredukowany do początkowego
stanu po instalacji wersji podstawowej (base system)
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>ustawienie ,,w zawieszeniu'' (,,on hold'')</term>
<listitem>
<para>
Dokonuje się tego klawiszem `=' i oznacza to ,,nie uaktualniaj, nawet jeśli
aktualnie zainstalowana wersja nie jest tak świeża, jak wersja ze źródła
pakietów, z jakiego korzystasz (jakie zostało skonfigurowane przy pomocy
opcji ,,Sposób dostępu''), pobrana po wyborze opcji <emphasis
role="strong">Uaktualnij</emphasis> (Update)).
</para>
<para>
Tak, jak możesz zawiesić uaktualnianie wersji danych pakietów, tak możesz
anulować zawieszenie poprzez naciśnięcie `:'.  Oznacza to zezwolenie na
uaktualnianie wersji pakietów, jeśli nowsze są dostępne.  Jest to domyślne
ustawienie dla pakietu.
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Możesz wybrać różne sposoby prezentacji listy pakietów, używając
klawisza ,,o'' do przełączania pomiędzy różnymi metodami sortowania
pakietów.  Domyślne sortowanie przeprowadzane jest według parametru
Priorytet.  Wewnątrz tego samego priorytetu, pakiety są poukładane według
katalogów (zwanych też sekcjami) archiwum, w których są przechowywane.  W
tym trybie, niektóre pakiety z sekcji (powiedzmy) A mogą być umieszczane
jako pierwsze, następnie niektóre pakiety z sekcji (powiedzmy) B, zaś po
nich znów pakiety z sekcji A, ale posiadające niższy priorytet.
</para>
<para>
Możesz również poszerzyć znaczenie etykiet na górze ekranu, wykorzystując
klawisz ,,v'' (ang.  verbose = rozwlekły).  Ta metoda przesuwa dużą cześć
widocznych poprzednio informacji poza prawą granicę ekranu.  Aby je
zobaczyć, naciśnij strzałkę w prawo, aby powrócić na lewo, możesz
skorzystać z klawisza strzałki w lewo.
</para>
<para>
Gdy zaznaczysz pakiet do instalacji lub do usunięcia, przykładowo <systemitem
role="package">foo.deb</systemitem>, i ten pakiet jest powiązany
zależnościami z innym pakietem, przykładowo <systemitem
role="package">blurf.deb</systemitem>, to <command>dselect</command> wyświetli
odpowiednie okno, w którym możesz wybrać spośród powiązanych pakietów,
akceptując sugerowaną akcję (instalować lub nie instalować, oto jest
pytanie) lub odrzucić je całkowicie.  Tą drugą czynność przeprowadzamy
klawiszami Shift+D.  Natomiast, aby powrócić do poprzedniego wyboru,
naciśnij Shift+U.  W każdym razie, możesz zapisać swój wybór i powrócić
do głównego ekranu kombinacją klawiszy Shift+Q.
</para>
</listitem>
<listitem>
<para>
Można wybrać menu ,,Instaluj'' (,,Install'') aby rozpakować i skonfigurować
zaznaczone pakiety lub usunąć pobrane pliki poleceniem ,,Usuń''
(,,Remove'').  Można także opuścić program wybierając polecenie ,,Wyjdź''
(,,Quit'').
</para>
</listitem>
</itemizedlist>
</section>

<section id="dpkg-deb"><title>dpkg-deb</title>
<para>
Ten program służy do pracy z plikami-archiwami Debiana
(<literal>*.deb</literal>).  Kilka najczęstszych zastosowań to:
</para>
<itemizedlist>
<listitem>
<para>
Pokaż wszystkie opcje: <literal>dpkg-deb --help</literal>.
</para>
</listitem>
<listitem>
<para>
Pokaż jakie pliki znajdują się w podanym archiwum: <literal>dpkg-deb
--contents foo_VVV-RRR.deb</literal>)
</para>
</listitem>
<listitem>
<para>
Wypakuj pliki zawarte w podanym archiwum do zadanego katalogu:
<literal>dpkg-deb --extract foo_VVV-RRR.deb KATALOG</literal> Polecenie
wypakuje pliki z <literal>foo_VVV-RRR.deb</literal> do katalogu
<literal>KATALOG/</literal>.  To wygodny sposób na zbadanie zawartości paczki
bez jej instalacji do głównego systemu plików.
</para>
</listitem>
</itemizedlist>
<para>
Zauważ, że dowolne pakiety, które były po prostu rozpakowane przy pomocy
<literal>dpkg-deb -extract</literal> nie zostały poprawnie zainstalowane.  W
celu instalacji należy użyć polecenia <literal>dpkg --install</literal>.
</para>
<para>
Więcej informacji na stronach podręcznika systemowego:
<citerefentry><refentrytitle>dpkg-deb</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
</para>
</section>

<section id="apt-get"><title>apt-get</title>
<para>
<command>apt-get</command> umożliwia prostą instalację pakietów z linii
poleceń.  W przeciwieństwie do <command>dpkg</command>,
<command>apt-get</command> nie obsługuje nazw plików .deb, ale pracuje w
oparciu o właściwe nazwy pakietów.  Korzysta ze źródeł pakietów
wymienionych w <filename>/etc/apt/sources.list</filename>.
</para>
<para>
Jeśli potrzebujesz więcej informacji, zainstaluj pakiet <systemitem
role="package">apt</systemitem> i przeczytaj strony podręcznika systemowego:
<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum></citerefentry>,
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
oraz opis <filename>/usr/share/doc/apt/guide.html/index.html</filename>.
</para>
</section>

<section id="dpkg-split"><title>dpkg-split</title>
<para>
Ten program dzieli duże pakiety na małe pliki (na przykład, w celu
umieszczenia na dyskietkach -- kto teraz używa dyskietek :-) ) oraz scala
takie podzielone pliki w jeden.  Może on być jedynie wykorzystany w systemie
opartym o Debiana (to jest takim, który zawiera pakiet <systemitem
role="package">dpkg</systemitem>), ponieważ uruchamia on inny program,
<literal>dpkg-deb</literal>, do rozbioru pliku pakietu Debiana na jego rekordy
składowe.
</para>
<para>
Dla przykładu, chcąc podzielić duży plik .deb na N kawałków:
</para>
<itemizedlist>
<listitem>
<para>
Wykonaj polecenie: <literal>dpkg-split --split foo.deb</literal>.  Wyprodukuje
ono N plików o przybliżonej wielkości 460 KBytes w aktualnym katalogu.
</para>
</listitem>
<listitem>
<para>
Skopiuj te N plików na dyskietki.  I idź do innego Debiana, albo pochodź
wokoło stołu i udawaj, że jesteś przy innym komputerze ;-).
</para>
</listitem>
<listitem>
<para>
Skopiuj zawartość dyskietek na twardy dysk na innej maszynie
</para>
</listitem>
<listitem>
<para>
Połącz kawałki używając polecenia <literal>dpkg-split --join
"foo*"</literal>.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="updaterunning"><title>Debian twierdzi, że można uaktualniać uruchomiony program; Jak to możliwe?</title>
<para>
Jądro (system plików) w Debianie umożliwia podmianę plików, nawet jeśli
są używane.
</para>
<para>
Dostarczamy także program o nazwie <command>start-stop-daemon</command>,
który jest używany do uruchamiania demonów w czasie startu systemu oraz do
ich zatrzymywania, podczas gdy poziom startu (runlevel) jest zmieniany.  Ten
sam program jest używany przez skrypty instalacyjne podczas instalacji nowego
pakietu zawierającego demona.
</para>
</section>

<section id="whatpackages"><title>Jak mogę sprawdzić, które pakiety są już zainstalowane w Debianie?</title>
<para>
Aby poznać stan wszystkich pakietów zainstalowanych w systemie, wykonaj
polecenie:
</para>
<screen>
dpkg --list
</screen>
<para>
Wypisze ono jednolinijkowe podsumowanie dla każdego pakietu, zawierające
dwuznakowe oznaczenie statusu (wyjaśnione w nagłówku), nazwę pakietu,
wersję, która jest <emphasis>zainstalowana</emphasis>, oraz krótki opis.
</para>
<para>
Aby poznać stan pakietów, których nazwy zawierają określony ciąg znaków,
możesz wykonać polecenie (to polecenie pokazuje pakiety z nazwami
rozpoczynającymi się od ,,foo''):
</para>
<screen>
dpkg --list 'foo*'
</screen>
<para>
Aby otrzymać obszerniejsze informacje na temat określonego pakietu, wykonaj:
</para>
<screen>
dpkg --status nazwa_pakietu
</screen>
</section>

<section id="filesearch"><title>Jak mam stwierdzić, który pakiet tworzy określony plik?</title>
<para>
Aby zidentyfikować pakiet odpowiedzialny za pojawienie się pliku
<literal>nazwa_pliku</literal> wykonaj:
</para>
<itemizedlist>
<listitem>
<para>
<literal>dpkg --search nazwa_pliku</literal>
</para>
<para>
To polecenie spowoduje wyszukanie pliku o nazwie <literal>nazwa_pliku</literal>
w zainstalowanych paczkach.  (Jest to ekwiwalent poszukiwania wszystkich
plików mających rozszerzenie <literal>.list</literal> i znajdujących się w
katalogu <literal>/var/lib/dpkg/info/</literal>, a następnie wypisywania nazw
wszystkich pakietów zawierających te pliki i odwrotnie).
</para>
</listitem>
<listitem>
<para>
<literal>zgrep foo Contents-ARCH.gz</literal>
</para>
<para>
To polecenie poszukuje plików zawierających tekst <literal>foo</literal> w
ich pełnej ścieżce dostępu.  Pliki <literal>Contents-ARCH.gz</literal>
(gdzie ARCH oznacza właściwą architekturę), znajdują się w głównym
katalogu z pakietami (main, non-free, contrib), na serwerze FTP Debiana.  Pliki
<literal>Contents</literal> odnoszą się tylko do pakietów z drzewa
pakietów, do którego należą.  Z tego powodu użytkownik może będzie
musiał przeszukać więcej niż jeden plik <literal>Contents</literal>, aby
znaleźć pakiet zawierający plik <literal>foo</literal>.
</para>
<para>
Ta metoda ma tą przewagę nad <literal>dpkg --search</literal>, że znajduje
dowolne pliki w dowolnych pakietach, nie tylko aktualnie zainstalowane w
systemie.
</para>
</listitem>
</itemizedlist>
</section>

</chapter>

