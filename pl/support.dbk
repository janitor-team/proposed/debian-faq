<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="support"><title>Pomoc dla systemu Debian GNU/Linux</title>
<section id="debiandocs"><title>Jakie inne dokumentacje istnieją dla systemu Debian GNU/Linux?</title>
<itemizedlist>
<listitem>
<para>
Instrukcja instalacji dla obecnego wydania - zobacz: <ulink
url="http://www.debian.org/releases/stable/installmanual">http://www.debian.org/releases/stable/installmanual</ulink>.
</para>
</listitem>
<listitem>
<para>
Podręcznik zasad zawiera normy dotyczące dystrybucji, takie jak: struktura i
zawartość archiwum Debiana, zagadnienia dotyczące projektowania systemu
operacyjnego itd.  Zawiera on również wymagania techniczne, jakie musi
spełnić każdy pakiet aby zostać włączonym do dystrybucji, oraz
dokumentację podstawowych aspektów technicznych pakietów binarnych i
źródłowych Debiana.
</para>
<para>
Pobierz go z pakietu: <systemitem role="package">debian-policy</systemitem> lub
z <ulink
url="http://www.debian.org/doc/devel-manuals#policy">http://www.debian.org/doc/devel-manuals#policy</ulink>.
</para>
</listitem>
<listitem>
<para>
Dokumentacja dotycząca zainstalowanych pakietów Debiana: Większość
pakietów posiada pliki pomocy, które zapisywane są do
<literal>/usr/doc/PAKIET</literal>.
</para>
</listitem>
<listitem>
<para>
Dokumentacja dla projektu Linux: Pakiety Debiana <systemitem
role="package">doc-linux</systemitem> zawierają wszystkie najnowsze wersje
HOWTO i mini-HOWTO z <ulink url="http://www.tldp.org/">Projektu Dokumentacji
Linuksa</ulink>.
</para>
</listitem>
<listitem>
<para>
Strony podręcznika systemowego `man' : Większość poleceń posiada strony
podręcznika systemowego napisane w postaci plików `man'.  Są one opisane
przez części indeksu `man' w którym się znajdują: n.p.  foo(3) odnosi się
do strony podręcznika, która znajduje się w /usr/share/man/man3/ i może
zostać wywołana przez wydanie polecenia: <literal>man 3 foo</literal>, lub po
prostu <literal>man foo</literal> jeśli trzecia część jest pierwszą
zawartą w podręczniku systemowym dotyczącym <literal>foo</literal>.
</para>
<para>
Można dowiedzieć się, który katalog z <literal>/usr/share/man/</literal>
zawiera konkretną stronę poprzez polecenie <literal>man -w foo</literal>.
</para>
<para>
Nowi użytkownicy Debiana powinni wiedzieć, że strony podręcznika
systemowego dla wielu poleceń nie są osiągalne, aż do czasu gdy
zainstalują pakiety:
</para>
<itemizedlist>
<listitem>
<para>
<literal>man-db</literal>, który zawiera program <literal>man</literal> oraz
inne programy do kontroli i sterowania stronami podręcznika systemowego.
</para>
</listitem>
<listitem>
<para>
<literal>manpages</literal>, który zawiera strony podręcznika systemowego.
(zobacz <xref linkend="nonenglish"/>).
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Strony `info': Dokumentacja użytkowników dotycząca wielu poleceń,
zwłaszcza narzędzi GNU, jest dostępna w postaci plików `info', a nie stron
podręcznika systemowego.  Pliki takie mogą być przeczytane przez narzędzie
GNU <literal>info</literal>, uruchamiane <literal>M-x info</literal> w GNU
Emacs lub przez inne przeglądarki.
</para>
<para>
Największą przewagą względem oryginalnych stron podręcznika systemowego
`man' jest hipertekstowy system stron info.  <emphasis>Nie</emphasis>
potrzebują one jednak przeglądarki WWW; <literal>info</literal> może zostać
uruchomione w zwykłej konsoli tekstowej.  Standard info został stworzony
przez Richarda Stallmana i poprzedzał WWW.
</para>
</listitem>
</itemizedlist>
<para>
Zapamiętaj, iż możesz mieć dostęp do dużej ilości dokumentacji w Twoim
systemie używając przeglądarki WWW, bezpośrednio, przez polecenia `dwww'
lub `dhelp', znajdujące się w poszczególnych pakietach.
</para>
</section>

<section id="onlineresources"><title>Czy w Internecie dostępne są jakieś możliwości do dyskusji na temat Debiana?</title>
<para>
Tak.  Poczta elektroniczna jest główną metodą wspierania użytkowników
Debiana.
</para>
<section id="s11.2.1"><title>Listy dyskusyjne.</title>
<para>
Jest ich wiele <ulink url="http://www.debian.org/MailingLists/">Listy
dyskusyjne związane z Debianem</ulink>.
</para>
<para>
Kompletny spis list dyskusyjnych znajduje się
<filename>/usr/share/doc/debian/mailing-lists.txt</filename> pod warunkiem, że
w systemie zainstalowany jest pakiet <systemitem
role="package">doc-debian</systemitem>.
</para>
<para>
Listy dyskusyjne Debiana nazywane są według następującego wzoru
debian-<replaceable>temat_listy</replaceable>.  Przykładem są
debian-announce, debian-user, debian-news.  Aby zapisać się na listę
dyskusyjną debian-<replaceable>temat_listy</replaceable>, wyślij list pocztą
elektroniczną do
debian-<replaceable>temat_listy</replaceable>-request@lists.debian.org wraz ze
słowem "subscribe" w temacie wiadomości.  Pamiętaj aby dodać
<emphasis>-request</emphasis> do adresu listu, gdy używasz tej metody
zapisywania się lub rezygnacji z listy dyskusyjnej.  W przeciwnym wypadku
Twój list elektroniczny zostanie wysłany na listę dyskusyjną.
</para>
<para>
Jeśli posiadasz przeglądarkę WWW obsługującą formularze możesz zapisać
się na listę dyskusyjną używając <ulink
url="http://www.debian.org/MailingLists/subscribe">Formularza WWW</ulink>.
Możesz również zrezygnować z listy używając <ulink
url="http://www.debian.org/MailingLists/unsubscribe">Formularza WWW</ulink>.
</para>
<para>
W razie jakiś problemów adres administratora list dyskusyjnych to:
<email>listmaster@lists.debian.org</email>
</para>
<para>
Archiwa list dyskusyjnych Debiana dostępne są pod adresem WWW: <ulink
url="http://lists.debian.org/">http://lists.debian.org/</ulink>.
</para>
<section id="mailinglistconduct"><title>Jakie zasady panują na listach dyskusyjnych?</title>
<para>
Gdy korzystasz z list dyskusyjnych związanych z Debianem postępuj zgodnie z
następującymi regułami:
</para>
<itemizedlist>
<listitem>
<para>
Nie wysyłaj spamu.  Zobacz <ulink
url="http://www.debian.org/MailingLists/#ads">Polityka rozpowszechniania list
dyskusyjnych związanych z Debianem</ulink>.
</para>
</listitem>
<listitem>
<para>
Nie przeklinaj; to niekulturalne.  Ludzie rozwijający Debiana to ochotnicy,
którzy poświęcają swój czas, energię i pieniądze próbując scalić
projekt Debian.
</para>
</listitem>
<listitem>
<para>
Nie używaj przekleństw; wielu ludzi otrzymuje listę za pomocą pakietu
radiowego, gdzie przeklinanie jest nielegalne.
</para>
</listitem>
<listitem>
<para>
Upewnij się czy używasz odpowiedniej listy.  <emphasis>Nie</emphasis>
wysyłaj swojej prośby o zapisanie lub rezygnację do listy dyskusyjnej
<footnote><para> .  Używaj do tego adresu
debian-<replaceable>temat_listy</replaceable>-REQUEST@lists.debian.org.
</para> </footnote>
</para>
</listitem>
<listitem>
<para>
Zobacz część <xref linkend="bugreport"/> aby dowiedzieć się więcej o
zgłaszaniu błędów.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="s11.2.2"><title>Opiekunowie pakietów.</title>
<para>
Użytkownicy mogą również adresować pytania do opiekunów poszczególnych
pakietów.  By zadać pytanie opiekunowi pakietu xyz, wyślij list pocztą
elektroniczną pod adres <emphasis>xyz@packages.debian.org</emphasis>.
</para>
</section>

<section id="s11.2.3"><title>Grupy dyskusyjne.</title>
<para>
Pytania nie dotyczące jednoznacznie Debiana użytkownicy powinni zadawać na
grupach dyskusyjnych związanych z Linuksem.  Są to grupy dyskusyjne
comp.os.linux.* lub linux.*.  Wiele adresów grup dyskusyjnych dotyczących
Linuksa oraz pokrewnych materiałów znaleźć możemy na stronach WWW : np.
<ulink url="http://www.linux.org/docs/usenet.html">Linux Online</ulink> lub
<ulink url="http://www.linuxjournal.com/helpdesk.php">LinuxJournal</ulink>.
</para>
</section>

</section>

<section id="searchtools"><title>Czy istnieje szybka metoda wyszukiwania materiałów związanych z systemem Debian GNU/Linux?</title>
<para>
Jest wiele wyszukiwarek, które pomagają szukać dokumentów związanych z
Debianem:
</para>
<itemizedlist>
<listitem>
<para>
<ulink url="http://search.debian.org/">Debian WWW search site</ulink>.
</para>
</listitem>
<listitem>
<para>
<ulink url="http://groups.google.com/">Google Groups</ulink>: przeszukuje listy
dyskusyjne.
</para>
<para>
Na przykład: aby dowiedzieć się więcej na temat sterowników do
kontrolerów Promise dla Debiana, spróbuj wyszukać wyrażenie
<literal>Promise Linux sterownik</literal>.  Zostaną znalezione wszystkie
wiadomości, które zawierają podany ciąg znaków, n.p.  dyskusje, których
tematem był ten problem.  Jeśli dodasz <literal>Debian</literal> do szukanego
wyrażenia, w wyniku otrzymasz wiadomości ściśle powiązane z Debianem.
</para>
</listitem>
<listitem>
<para>
Zwykłe wyszukiwarki, takie jak: <ulink
url="http://www.altavista.com/">AltaVista</ulink> lub <ulink
url="http://www.google.com/">Google</ulink> jeśli będziesz używać
prawidłowych warunków wyszukiwania.
</para>
<para>
Na przykład: wyszukanie terminu "cgi-perl" dostarcza o wiele dokładniejsze
wyjaśnienia, niż krótki opis zawarty w tym pakiecie.
</para>
</listitem>
</itemizedlist>
</section>

<section id="buglogs"><title>Czy istnieją logi znanych błędów?</title>
<para>
Dystrybucja Debian GNU/Linux posiada "System Śledzenia Błędów - Bug
Tracking System (BTS)", który przechowuje informacje o błędach zgłoszonych
przez użytkowników i opiekunów pakietów.  Każdy błąd otrzymuje swój
numer i przechowywany jest w pliku tak długo, aż nie zostanie uznany za
usunięty.
</para>
<para>
Kopie tych informacji są dostępne pod adresem <ulink
url="http://www.debian.org/Bugs/">http://www.debian.org/Bugs/</ulink>.
</para>
<para>
Dostęp do bazy danych "Systemu Śledzenia Błędów (BTS)" zapewnia serwer
pocztowy.  Aby dowiedzieć się więcej, wyślij list drogą elektroniczną
zawierający słowo "help" na adres: request@bugs.debian.org.
</para>
</section>

<section id="bugreport"><title>Jak mogę zgłosić błąd dotyczący Debiana?</title>
<para>
Jeśli znaleziono błąd w Debianie, postępuje się zgodnie z instrukcją
zgłaszania błędów dotyczących Debiana.  Instrukcję tę można otrzymać
na kilka sposobów:
</para>
<itemizedlist>
<listitem>
<para>
Poprzez anonimowy FTP.  Strony serwerów lustrzanych Debiana zawierają
instrukcję w pliku <literal>doc/bug-reporting.txt</literal>.
</para>
</listitem>
<listitem>
<para>
Poprzez stronę WWW.  Kopia instrukcji znajduje się pod adresem <ulink
url="http://www.debian.org/Bugs/Reporting">http://www.debian.org/Bugs/Reporting</ulink>.
</para>
</listitem>
<listitem>
<para>
W każdej dystrybucji Debiana poprzez instalację pakietu <systemitem
role="package">doc-debian</systemitem>.  Instrukcja znajduje się w pliku
<filename>/usr/doc/debian/bug-reporting.txt</filename>.
</para>
</listitem>
</itemizedlist>
<para>
Możesz również użyć pakietu <systemitem role="package">bug</systemitem>
lub <systemitem role="package">reportbug</systemitem>, które poprowadzą Cię
przez proces zgłaszania błędów i wyślą wiadomość o nich pod odpowiedni
adres wraz z dodatkowymi informacjami, dotyczącymi Twojego systemu.
</para>
<para>
Jeśli chcesz wysłać raport o błędzie z programu pocztowego, wyślij
wiadomość do <email>submxit@bugs.debian.org</email>.  Pierwsza linia
wiadomości musi zawierać:
</para>
<screen>
Package: nazwa pakietu
</screen>
<para>
Następna linia powinna zawierać wersję pakietu podaną w podobny sposób:
</para>
<screen>
Version: numer wersji
</screen>
<para>
Numer wersji dla jakiegokolwiek pakietu zainstalowanego w Twoim systemie można
uzyskać wydając polecenie:
</para>
<screen>
dpkg -s <replaceable>nazwa pakietu</replaceable>
</screen>
<para>
Reszta wiadomości powinna zawierać dokładny opis błędu, dystrybucji
Debiana jakiej używasz oraz numery wersji innych stosowanych pakietów.
Wersję dystrybucji Debiana, którą obecnie używasz, można wyświetlić
poprzez polecenie
</para>
<screen>
cat /etc/debian_version
</screen>
<para>
.
</para>
<para>
Po wysłaniu przez Ciebie zgłoszenia błędu zostaniemy automatycznie
powiadomieni o otrzymaniu zgłoszenia.  Również automatycznie błąd otrzyma
numer (zapisany w logu błędów) oraz zostanie wysłany na listę dyskusyjną
debian-bugs-dist.
</para>
<para>
Jeśli znaleziony błąd jest powiązany z wieloma pakietami, zamiast wysyłać
wiele podobnych zgłoszeń błędu, należy wysłać zgłoszenia odnośnie
błędów do <email>maintonly@bugs.debian.org</email> (zamiast adresu
submit@...  ), aby powiadomić tylko opiekunów poszczególnych pakietów, a
następnie wysłać podsumowanie zgłoszonych błędów do listy mailingowej
debian-devel lub debian-bugs-dist.
</para>
<para>
Dodatkowo istnieje tester pakietów Debiana nazywany <ulink
url="http://www.debian.org/lintian/">Lintian</ulink>.  Został on tak
zaprojektowany, aby automatycznie przeszukiwać pakiety w celu znalezienia
naruszeń zasad dystrybucji czy błędów wykrytych już wcześniej w innych
pakietach.  Zatem jeśli znajdziesz błąd, który może również wystąpić w
innych pakietach, lepiej będzie jeśli skontaktujesz się z opiekunami pakietu
Lintian <email>lintian-maint@debian.org</email>.  Powstanie nowej metody
wyszukiwania błędów zapobiegnie pojawianiu się tego samego błędu w innych
pakietach dystrybucji.
</para>
<para>
Możesz również używać adresu <email>quiet@bugs.debian.org</email>, aby
zgłaszać błędy tylko do "Systemu Śledzenia Błędów (BTS)", bez
jednoczesnego wysyłania ich do listy dyskusyjnej debian-bugs-dist lub do
opiekuna pakietu.  Adres ten jest wykorzystywany bardzo rzadko np.  jeśli
chcesz dołączyć drugorzędne dane do swojego zgłoszenia błędu lub gdy
chcesz zaznaczyć coś w logu "Systemu Śledzenia Błędów (BTS)", a już
wcześniej powiadomiono o tym opiekuna projektu.
</para>
</section>

</chapter>

