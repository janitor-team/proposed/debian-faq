<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="getting"><title>Zdobywanie i instalacja systemu Debian GNU/Linux</title>
<section id="version"><title>Jaka jest najnowsza wersja Debiana?</title>
<para>
Obecnie istnieją trzy wersje systemu Debian GNU/Linux:
</para>
<variablelist>
<varlistentry>
<term><emphasis>wersja 9, znana także jako dystrybucja `stabilna'</emphasis></term>
<listitem>
<para>
Jest to stabilne i dobrze przetestowane oprogramowanie, zmieniające się,
jeśli poprawiono jego bezpieczeństwo lub użyteczność.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>dystrybucja `testowa'</emphasis></term>
<listitem>
<para>
Tutaj są umieszczone pakiety, które znajdą się w następnym wydaniu
`stabilnym'; zostały przetestowane w dystrybucji niestabilnej, ale jeszcze nie
nadają się do wydania.  Ta dystrybucja jest uaktualniana częściej niż
`stabilna', ale rzadziej niż `niestabilna'.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>dystrybucja `niestabilna'</emphasis></term>
<listitem>
<para>
Ta wersja jest aktualnie w fazie rozwoju, jest stale uaktualniana.  Możesz
pobrać pakiety z archiwum wersji `niestabilnej' na dowolnej witrynie FTP
Debiana i użyć ich do uaktualnienia swojego systemu w każdej chwili, ale nie
powinieneś oczekiwać, że system będzie nadawał się do użytku w takim
samym stopniu jak poprzednio lub będzie tak samo stabilny - oto dlaczego jest
ona nazywana `<emphasis role="strong">niestabilną</emphasis>'!
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Chcąc uzyskać więcej informacji zobacz <xref linkend="dists"/>.
</para>
</section>

<section id="boot-floppies"><title>Gdzie/jak mogę zdobyć płyty instalacyjne Debiana?</title>
<para>
Obrazy płyt instalacyjnych możesz zdobyć pobierając odpowiednie pliki z
jednego z <ulink url="http://www.debian.org/mirror/list">serwerów lustrzanych
Debiana</ulink>.
</para>
<para>
Pliki instalacyjne systemu są wydzielone w podkatalogach katalogu
<filename>dists/stable/main</filename> , a nazwy tych podkatalogów odnoszą
się do Twojej architektury, w następujący sposób:
<literal>disks-<replaceable>arch</replaceable></literal>
(<replaceable>arch</replaceable> to "i386", "sparc", itd, sprawdź witrynę,
aby uzyskać dokładną listę).  W każdym z podkatalogów dla danej
architektury może znajdować się kilka katalogów, każdy dla wersji
instalacyjnej systemu, a aktualnie używana znajduje się w katalogu `current'
(który jest dowiązaniem symbolicznym).
</para>
<para>
Zobacz plik <filename>README.txt</filename> w tym katalogu w celu uzyskania
dalszych instrukcji.
</para>
</section>

<section id="cdrom"><title>Jak mogę zainstalować Debiana z płyt CD-ROM?</title>
<para>
Linux obsługuje system plików ISO 9660 (CD-ROM) z rozszerzeniami Rock Ridge
(dawniej znany jako "High Sierra").  Kilku <ulink
url="http://www.debian.org/CD/vendors/">sprzedawców</ulink> dostarcza system
Debian GNU/Linux w tym formacie.
</para>
<para>
Ostrzeżenie: Kiedy instalujesz system z płyt CD-ROM, zazwyczaj nie jest
dobrym pomysłem wybór metody dostępu do <literal>cdrom</literal> przy pomocy
narzędzia dselect.  Metoda ta jest zazwyczaj bardzo wolna.  Na przykład
metody <literal>mountable</literal> i <literal>apt</literal> są znacznie
lepsze przy instalacji z CD-ROMów (zobacz <xref linkend="dpkg-mountable"/> i
<xref linkend="apt"/>).
</para>
</section>

<section id="cdimages"><title>Mam nagrywarkę płyt CD, czy są gdzieś dostępne obrazy płyt CD?</title>
<para>
Tak.  Aby ułatwić sprzedawcom płyt CD dostarczanie dysków wysokiej
jakości, udostępniamy <ulink url="http://cdimage.debian.org/">oficjalne
obrazy płyt CD</ulink>.
</para>
</section>

<section id="floppy"><title>Czy mogę zainstalować go z dyskietek?</title>
<para>
Najpierw ostrzeżenie: cały Debian GNU/Linux jest zdecydowanie zbyt duży, aby
instalować go ze standardowych dyskietek 1.44 MB - instalacji takiej raczej
nie nazwałbyś bardzo przyjemnym doświadczeniem.
</para>
<para>
Skopiuj pakiety Debiana na sformatowane dyskietki.  Obsługiwane formaty to
DOS, rodzimy format Linuksa "ext2" lub "minix"; nie zapomnij użyć polecenia
mount właściwego dla użytego formatu dyskietki.
</para>
<para>
Użycie dyskietek powoduje następujące komplikacje:
</para>
<itemizedlist>
<listitem>
<para>
Krótkie nazwy plików MS-DOS.  Jeśli próbujesz umieścić pakiety Debiana na
dyskietkach formatu MS-DOS, stwierdzisz, że ich nazwy zazwyczaj są za długie
i nie odpowiadają ograniczeniom długości nazw plików MS-DOS do 8.3.  Aby
rozwiązać ten problem, powinieneś użyć dysków z systemem plików VFAT,
ponieważ VFAT obsługuje długie nazwy plików.
</para>
</listitem>
<listitem>
<para>
Duże rozmiary plików: niektóre pakiety posiadają rozmiar większy niż 1,44
MB i nie zmieszczą się na jednej dyskietce.  Do rozwiązania tej
niedogodności użyj narzędzia dpkg-split (zobacz <xref
linkend="dpkg-split"/>), dostępnego w katalogu <literal>tools</literal>, na
<ulink url="http://www.debian.org/mirror/list">serwerach lustrzanych
Debiana</ulink>.
</para>
</listitem>
</itemizedlist>
<para>
Chcąc odczytywać i zapisywać dane z dyskietki, musisz posiadać obsługę
dyskietek wkompilowaną w jądro; większość jąder wspiera obsługę
napędów dyskietek.
</para>
<para>
Chcąc zamontować dyskietkę w punkcie montowania <literal>/floppy</literal>
(katalog powinien zostać utworzony w czasie instalacji) wpisz
</para>
<itemizedlist>
<listitem>
<screen>
mount -t msdos /dev/fd0 /floppy/
</screen>
<para>
jeżeli dyskietka znajduje się w napędzie A: i posiada system plików MS-DOS,
</para>
</listitem>
<listitem>
<screen>
mount -t msdos /dev/fd1 /floppy/
</screen>
<para>
jeżeli dyskietka znajduje się w napędzie B: i posiada system plików MS-DOS,
</para>
</listitem>
<listitem>
<screen>
mount -t ext2 /dev/fd0 /floppy/
</screen>
<para>
jeżeli dyskietka znajduje się w napędzie A: i posiada system plików ext2
(tj.  podstawowy system plików Linuksa).
</para>
</listitem>
</itemizedlist>
</section>

<section id="remoteinstall"><title>Czy mogę pobrać i zainstalować Debiana bezpośrednio z Internetu?</title>
<para>
Tak.  Możesz zainicjować system instalacyjny Debiana z zestawu plików,
który możesz pobrać z naszego serwera FTP i jego serwerów lustrzanych.
</para>
<para>
Możesz pobrać mały plik z obrazem płyty CD, utworzyć z niego płytę
startową, zainstalować z niej system podstawowy, a resztę poprzez sieć.
Aby uzyskać więcej informacji zobacz <ulink
url="http://www.debian.org/CD/netinst/">http://www.debian.org/CD/netinst/</ulink>.
</para>
<para>
Możesz także pobrać pliki z obrazami dyskietek, utworzyć z nich dyskietki
startowe, uruchomić procedurę instalacyjną, a resztę Debiana pobrać
poprzez sieć.  Aby uzyskać więcej informacji zobacz <ulink
url="http://www.debian.org/distrib/floppyinst">http://www.debian.org/distrib/floppyinst</ulink>.
</para>
</section>

</chapter>

