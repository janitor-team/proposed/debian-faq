<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="pkg-basics"><title>Podstawy zarządzania systemem pakietów Debiana</title>
<section id="package"><title>Czym jest pakiet w systemie Debian?</title>
<para>
Pakiety ogólnie zawierają wszystkie pliki potrzebne aby zaimplementować
zbiór powiązanych poleceń lub cech.  Są dwa typy pakietów w Debianie:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>Pakiety binarne</emphasis>, które zawierają pliki wykonywalne,
konfiguracyjne , strony man oraz info, informacje o prawach autorskich i inną
dokumentację.  Pakiety te są rozpowszechniane w specyficznym dla systemu
Debian formacie archiwum (zobacz <xref linkend="deb-format"/>); wyróżniane
są zazwyczaj przez rozszerzenie pliku '.deb'.  Pakiety binarne mogą być
rozpakowane narzędziem systemu Debian <literal>dpkg</literal>; szczegóły są
opisane w tym dokumencie.
</para>
</listitem>
<listitem>
<para>
<emphasis>Pakiety źródłowe</emphasis>, które składają się z pliku
<literal>.dsc</literal> opisującego źródłowy pakiet (włącznie z nazwami
następnych plików) , <literal>.orig.tar.gz</literal> zawierający oryginalne,
niezmodyfikowane źródło w skompresowanym gzipem archiwum tar i plik
<literal>.diff.gz</literal> zawierający zazwyczaj zmiany związane z systemem
Debian wprowadzone w oryginalnym źródle.  Narzędzie
<literal>dpkg-source</literal> pakuje i rozpakowuje archiwa źródłowe systemu
Debian; szczegóły są zawarte w tym dokumencie.
</para>
</listitem>
</itemizedlist>
<para>
Instalacja oprogramowania przez system pakietów używa "zależności", które
są ostrożnie projektowane przez opiekunów pakietów.  Te zależności są
udokumentowane w pliku <literal>control</literal> związanym z każdym
pakietem.  Dla przykładu, pakiet zawierający kompilator GNU C (<systemitem
role="package">gcc</systemitem> "zależy" od pakietu <systemitem
role="package">binutils</systemitem>, który zawiera konsolidator i asembler.
Jeżeli użytkownik spróbuje zainstalować <systemitem
role="package">gcc</systemitem> bez wcześniej zainstalowanego <systemitem
role="package">binutils</systemitem>, system zarządzania pakietami (dpkg)
wyśle wiadomość z błędem, że potrzeba jeszcze <systemitem
role="package">binutils</systemitem> i zatrzyma instalację <systemitem
role="package">gcc</systemitem>.  (Jednak, ułatwienie to może być pominięte
przez upartego użytkownika, zobacz
<citerefentry><refentrytitle>dpkg</refentrytitle><manvolnum>8</manvolnum></citerefentry>.)
Zobacz więcej w <xref linkend="depends"/> poniżej.
</para>
<para>
Narzędzia pakietowe systemu Debian mogą być użyte do:
</para>
<itemizedlist>
<listitem>
<para>
manipulowania i zarządzania pakietami lub częściami pakietów,
</para>
</listitem>
<listitem>
<para>
jako pomoc dla użytkownika przy rozłożeniu pakietów, które muszą być
przeniesione za pomocą ograniczonego rozmiarem nośnika takiego, jak
dyskietka,
</para>
</listitem>
<listitem>
<para>
pomocy wykonawcom w tworzeniu archiwum pakietów, i
</para>
</listitem>
<listitem>
<para>
pomocy użytkownikom w instalacji pakietów, które znajdują się na zdalnym
serwerze FTP.
</para>
</listitem>
</itemizedlist>
</section>

<section id="deb-format"><title>Jaki jest format pakietu binarnego w systemie Debian?</title>
<para>
"Pakiet" systemu Debian lub plik archiwum systemu Debian zawiera pliki
wykonywalne, biblioteki i dokumentację, związane z poszczególnymi
częściami programu lub zbiorem powiązanych programów.  Normalnie, plik
archiwum systemu Debian ma nazwę, która kończy się na
<literal>.deb</literal>.
</para>
<para>
Wewnętrzny format pakietu binarnego Debiana jest opisany w instrukcji
<citerefentry><refentrytitle>deb</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
Ten wewnętrzny format jest tematem zmian (pomiędzy głównymi wydaniami
systemu Debian GNU/Linux), dlatego zawsze używaj
<citerefentry><refentrytitle>dpkg-deb</refentrytitle><manvolnum>1</manvolnum></citerefentry>
do manipulowania plikami <literal>.deb</literal> .
</para>
</section>

<section id="pkgname"><title>Dlaczego nazwy plików pakietów w systemie Debian są tak długie?</title>
<para>
Nazwy plików pakietów binarnych systemu Debian są podporządkowane
następującej konwencji:
&lt;foo&gt;_&lt;VersionNumber&gt;-&lt;DebianRevisionNumber&gt;.deb
</para>
<para>
Zauważ, że <literal>foo</literal> jest zobowiązane być nazwą pakietu.
Można poznać nazwę pakietu, związanego ze szczególnym plikiem archiwum
systemu Debian (.deb file), wykorzystując do sprawdzenia jeden z
następujących sposobów:
</para>
<itemizedlist>
<listitem>
<para>
obejrzyj plik "Packages" w katalogu, gdzie jest on przechowany na stronie FTP
archiwum systemu Debian.  Ten plik zawiera zwrotkę opisującą każdy pakiet;
pierwsze pole w każdej zwrotce jest formalną nazwą pakietu.
</para>
</listitem>
<listitem>
<para>
użyj polecenia <literal>dpkg --info foo_VVV-RRR.deb</literal> (gdzie VVV i RRR
są odpowiednio wersją i poprawką pakietu w pytaniu).  Polecenie to
wyświetli, pomiędzy innymi rzeczami, rozwiniętą nazwę pakietu
odpowiadającą plikowi archiwum.
</para>
</listitem>
</itemizedlist>
<para>
Część <literal>VVV</literal> jest numerem wersji ustanawianym przez
głównego programistę.  Nie ma standardów w tym miejscu, więc numer wersji
może posiadać format tak zróżnicowany jak "19990513" i "1.3.8pre1".
</para>
<para>
Część <literal>RRR</literal> jest numerem weryfikacji w systemie Debian, i
jest ustalany przez dewelopera Debiana (lub indywidualnego użytkownika jeśli
wybierze zbudowanie pakietu samemu).  Ten numer zgadza się z poziomem
weryfikacji pakietu systemu Debian, dlatego nowy poziom weryfikacji zazwyczaj
oznacza zmiany w pliku Makefile (<literal>debian/rules</literal>), pliku
kontroli (<literal>debian/control</literal>), skryptach instalacyjnych i
usuwających (<literal>debian/p*</literal>), lub w plikach konfiguracyjnych
użytych w pakiecie.
</para>
</section>

<section id="controlfile"><title>Czym jest plik kontroli w pakiecie systemu Debian?</title>
<para>
Szczegóły dotyczące treści pliku kontroli są zawarte w instrukcji Debian
Packaging, rozdział 4, zobacz <xref linkend="debiandocs"/>.
</para>
<para>
W skrócie, przykładowy plik kontroli jest pokazany poniżej dla pakietu
systemu Debian hello:
</para>
<screen>
Package: hello
Priority: optional
Section: devel
Installed-Size: 45
Maintainer: Adam Heath &lt;doogie@debian.org&gt;
Architecture: i386
Version: 1.3-16
Depends: libc6 (>= 2.1)
Description: The classic greeting, and a good example
 The GNU hello program produces a familiar, friendly greeting.  It
 allows nonprogrammers to use a classic computer science tool which
 would otherwise be unavailable to them.
 .
 Seriously, though: this is an example of how to do a Debian package.
 It is the Debian version of the GNU Project's `hello world' program
 (which is itself an example for the GNU Project).
</screen>
<para>
Pole Package zawiera nazwę pakietu.  To jest nazwa, za pomocą której pakiet
może być przetwarzany przez narzędzia pakietowe i zazwyczaj jest podobna,
ale nie koniecznie taka sama, jak pierwsza część nazwy pliku archiwum
systemu Debian.
</para>
<para>
Pole Version zawiera numer wersji nadany przez głównego programistę i (w
ostatniej części) poziom weryfikacji programu w pakiecie systemu Debian, tak
jak jest to wyjaśnione w <xref linkend="pkgname"/>.
</para>
<para>
Pole Architecture określa chip, dla którego ten konkretny pakiet binarny
został skompilowany.
</para>
<para>
Pole Depends podaje listę pakietów, które muszą być zainstalowane w
podanej kolejności, aby z sukcesem zainstalować pakiet.
</para>
<para>
Pole Installed-Size wskazuje ile miejsca na dysku zabierze zainstalowany
pakiet.  Pole to jest przeznaczone do użytku przez programy instalujące, aby
pokazać czy jest dostępna wystarczająca ilość miejsca na dysku by
zainstalować program.
</para>
<para>
Linia Section podaje nazwę "sekcji", czyli gdzie ten pakiet systemu Debian
jest przechowywany na stronach FTP systemu Debian.  Jest to nazwa podkatalogu
(wewnątrz jednego z głównych katalogów, zobacz <xref linkend="dirtree"/>),
gdzie pakiet jest przechowywany.
</para>
<para>
Pole Priority wskazuje jak ważny jest ten pakiet dla instalacji tak, aby
pseudo-inteligentne programy jak dselect lub console-apt mogły sortować
pakiety w kategorie np.  pakiety opcjonalnie instalowane.  Zobacz <xref
linkend="priority"/>.
</para>
<para>
Pole Maintainer zawiera adres e-mail osoby, która jest aktualnie
odpowiedzialna za utrzymywanie tego pakietu.
</para>
<para>
Pole Description zawiera krótkie podsumowanie cech pakietu.
</para>
<para>
Więcej informacji o wszystkich możliwych polach jakie może mieć pakiet,
proszę zobacz Debian Packaging Manual, rozdział 4., "Control files and their
fields".
</para>
</section>

<section id="conffile"><title>Czym jest plik conffile w systemie Debian?</title>
<para>
Conffiles jest listą plików konfiguracyjnych (zazwyczaj umieszczonych w
<literal>/etc</literal>), których system zarządzania pakietami nie będzie
nadpisywał gdy pakiet będzie aktualizowany.  To gwarantuje, że zawartość
tych plików będzie zachowana i jest krytycznie ważną cechą,
umożliwiającą aktualizację pakietu w działającym systemie.
</para>
<para>
Aby ustalić dokładnie, które pliki są zachowywane podczas aktualizacji
pakietu, uruchom:
</para>
<screen>
dpkg --status pakiet
</screen>
<para>
I zobacz poniżej "Conffiles:".
</para>
</section>

<section id="maintscripts"><title>Czym są w systemie Debian skrypty: preinst, postinst, prerm i postrm ?</title>
<para>
Te pliki są wykonywalnymi skryptami, które są automatycznie uruchamiane
przed lub po instalacji pakietu.  Wraz z plikiem o nazwie
<literal>control</literal>, wszystkie z tych plików są częścią sekcji
"control" w pliku archiwum systemu Debian.
</para>
<para>
Indywidualne pliki to:
</para>
<variablelist>
<varlistentry>
<term>preinst</term>
<listitem>
<para>
Ten skrypt jest uruchomiony przed rozpakowaniem pakietu z jego pliku archiwum
(".deb").  Wiele skryptów 'preinst' zatrzymuje usługi dla pakietów, które
będą aktualizowane, dopóki ich instalacja lub aktualizowanie się nie
zakończy (następny, po pomyślnym wykonaniu skryptu 'preinst' jest skrypt
'postinst').
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>postinst</term>
<listitem>
<para>
Ten skrypt zazwyczaj kończy jakiekolwiek wymagane konfiguracje pakietu
<literal>foo</literal>, który był już rozpakowany z jego pliku archiwum
(".deb").  Często skrypt 'postinst' prosi użytkownika o wprowadzenie
informacji, i/lub ostrzega użytkownika, że jeśli zaakceptuje domyślne
wartości, powinien pamiętać o tym aby przywrócić i zrekonfigurować pakiet
jeśli wymaga tego sytuacja.  Wiele skryptów 'postinst' wykonuje potem
polecenia potrzebne do uruchomienia lub ponownego uruchomienia usługi po tym,
jak nowy pakiet został zainstalowany.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>prerm</term>
<listitem>
<para>
Ten skrypt zazwyczaj zatrzymuje różne procesy powiązane z pakietem.  Jest
uruchamiany przed usunięciem plików powiązanych z pakietem.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>postrm</term>
<listitem>
<para>
Ten skrypty zazwyczaj modyfikuje powiązania lub inne pliki związane z
<literal>foo</literal> i/lub usuwa pliki stworzone przez pakiet.  (Zobacz też
<xref linkend="virtual"/>.)
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Obecnie wszystkie pliki control można znaleźć w katalogu
<literal>/var/lib/dpkg/info</literal>.  Pliki związane z pakietem
<literal>foo</literal> zaczynają się od "foo" i mają rozszerzenie
odpowiednio "preinst", "postinst", itd.  .  Plik <literal>foo.list</literal> w
tym katalogu zawiera listę wszystkich plików, które były zainstalowane z
pakietem <literal>foo</literal>.  (Zauważ, że pliki te zlokalizowane są
wewnątrz katalogu związanego z programem dpkg; nie powinieneś polegać na
nim.)
</para>
</section>

<section id="priority"><title>Co to jest pakiet <emphasis>Wymagany</emphasis>, <emphasis>Ważny</emphasis>, <emphasis>Standardowy</emphasis>, <emphasis>Opcjonalny</emphasis> lub <emphasis>Dodatkowy</emphasis>?</title>
<para>
Każdy pakiet systemu Debian ma przydzielony <emphasis>priorytet</emphasis>
przez opiekunów dystrybucji, jako pomoc dla systemu zarządzania pakietami.
Priorytety to:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis role="strong">Wymagany</emphasis>: pakiety które są konieczne dla
samego funkcjonowania systemu.
</para>
<para>
Zawiera wszystkie niezbędne narzędzia do naprawy wad w systemie.  Nie możesz
usuwać tych pakietów, ponieważ Twój system może stać się całkowicie
zepsuty i może być nawet niemożliwe użycie programu dpkg, aby to
odwrócić.  Systemy z samymi pakietami wymaganymi są prawdopodobnie
nieużyteczne, ale mają wystarczającą funkcjonalność, aby umożliwić
administratorowi systemu uruchomienie i instalację programów.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Ważny</emphasis> Te pakiety powinny się znaleźć w
każdym systemie rodziny Unix.
</para>
<para>
Są to inne pakiety, bez których system nie będzie działał dobrze lub nie
będzie użyteczny.  Tutaj <emphasis>NIE SĄ</emphasis> zawarte wielkie
aplikacje jak np.  Emacs, X11 lub TeX.  Te pakiety stanowią tylko samą
infrastrukturę.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Standardowy</emphasis> Te pakiety są standardowe w
każdym systemie Linux, włączając dość małe systemy, ale też nie są
ograniczone do systemów znakowych.
</para>
<para>
Są to pakiety, które będą zainstalowane domyślnie jeśli użytkownik nie
wybierze niczego innego.  Nie są tu zawarte duże aplikacje, ale znajduje się
tutaj Emacs (jest to bardziej część infrastruktury niż aplikacja) i spora
część TeX i LaTeX (jeśli to możliwe, aby były dostępne bez systemu
X-windows).
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Opcjonalny</emphasis> Zawarte tu są te wszystkie
pakiety, które możesz chcieć zainstalować, jeśli nie wiesz czym one są
lub nie masz sprecyzowanych wymagań.
</para>
<para>
Znajdują się tu X11, pełna dystrybucja TeX i dużo innych aplikacji.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Dodatkowy</emphasis>: Występują tu pakiety, które
powodują konflikt z innymi pakietami posiadającymi wyższy priorytet, są
zapewnie użyteczne jeśli już wiesz czym one są, albo sprecyzowałeś
wymagania, które czynią te pakiety nieodpowiednie dla "Opcjonalny".
</para>
</listitem>
</itemizedlist>
</section>

<section id="virtual"><title>Co to jest wirtualny pakiet?</title>
<para>
Wirtualny pakiet jest ogólną nazwą, która odnosi się do jednej z grupy
pakietów, w której każdy z pakietów dostarcza podobnej podstawowej
funkcjonalności.  Dla przykładu oba programy <literal>tin</literal> i
<literal>trn</literal> są czytnikami wiadomości i dlatego powinny spełnić
zależności programów, które wymagają czytnika wiadomości w systemie, aby
pracować lub być użytecznym.  Dlatego też o obu programach można
powiedzieć, że dostarczają "wirtualny pakiet" nazwany
<literal>news-reader</literal>.
</para>
<para>
Podobnie <literal>smail</literal> i <literal>sendmail</literal>, oba
dostarczają funkcjonalności agenta transportu wiadomości.  Oba mogą
powiedzieć że dostarczają wirtualny pakiet "agent transportu wiadomości".
Jeśli jeden z nich jest zainstalowany, wtedy dowolny program zależny od
instalacji <literal>mail-transport-agent</literal> będzie usatysfakcjonowany
przez istnienie wirtualnego pakietu.
</para>
<para>
System Debian dostarcza mechanizm, który, jeśli jest zainstalowany więcej
niż jeden pakiet dostarczający ten sam wirtualny pakiet, pozwala
administratorowi systemu ustawić jeden preferowany pakiet.  Odpowiednie
polecenie to <literal>update-alternatives</literal> i jest opisane dalej w
<xref linkend="diverse"/>.
</para>
</section>

<section id="depends"><title>Co oznacza powiedzenie, że pakiet <emphasis>Zależy</emphasis>, <emphasis>Zaleca</emphasis>, <emphasis>Proponuje</emphasis>, <emphasis>Wchodzi w konflikt</emphasis>, <emphasis>Zastępuje</emphasis> lub <emphasis>Dostarcza</emphasis> inny pakiet?</title>
<para>
System pakietów Debiana posiada zasięg "zależności" pakietu, który jest
zaprojektowany aby wskazywać (w pojedynczej fladze) poziom, na którym Program
A może działać niezależnie od istnienia Programu B w danym systemie:
</para>
<itemizedlist>
<listitem>
<para>
Pakiet A <emphasis>zależy</emphasis> od Pakietu B, jeśli B absolutnie musi
być w kolejności zainstalowany aby uruchomić A.  W pewnych przypadkach A
zależy nie tylko od B, ale od wersji B.  W tym przypadku zależność wersji
jest zazwyczaj dolnym limitem, w tym sensie że A zależy od dowolnej wersji B
większej niż pewna sprecyzowana wersja.
</para>
</listitem>
<listitem>
<para>
Pakiet A <emphasis>zaleca</emphasis> Pakiet B, jeśli opiekun pakietu oceni,
że większość użytkowników nie będzie chciała A bez posiadania
funkcjonalności dostarczanej przez B.
</para>
</listitem>
<listitem>
<para>
Pakiet A <emphasis>proponuje</emphasis> Pakiet B, jeśli B zawiera pliki,
które są powiązane (i zazwyczaj poszerzają) funkcjonalność A.
</para>
</listitem>
<listitem>
<para>
Pakiet A <emphasis>wchodzi w konflikt</emphasis> z Pakietem B, gdy A nie
będzie działać, jeśli B jest zainstalowany w systemie.  Najczęściej
konflikty są przypadkami gdzie A zawiera pliki, które posiadają ulepszenia w
stosunku do plików w B.  "wchodzi w konflikt" jest często połączone z
"zastępuje".
</para>
</listitem>
<listitem>
<para>
Pakiet A <emphasis>zastępuje</emphasis> Pakiet B, kiedy pliki instalowane
przez pakiet B są usuwane i (w pewnych przypadkach) nadpisywane przez pliki z
A.
</para>
</listitem>
<listitem>
<para>
Pakiet A <emphasis>dostarcza</emphasis> Pakiet B, kiedy wszystkie z plików i
działań pakietu B są włączone w A.  Ten mechanizm umożliwia użytkownikom
z ograniczonym miejscem na dysku pobranie tylko tych części pakietu A, które
są naprawdę potrzebne.
</para>
</listitem>
</itemizedlist>
<para>
Bardziej szczegółowe informacje o użyciu tych terminów można znaleźć w
Packaging manual i Policy manual.
</para>
</section>

<section id="pre-depends"><title>Co rozumie się przez Pre-Depends?</title>
<para>
"Pre-Depends" jest specjalną zależnością.  W przypadku wielu pakietów,
<literal>dpkg</literal> rozpakuje ich pliki archiwum (np.  są to pliki
<literal>.deb</literal>) niezależnie od tego, czy pliki od których one
zależą istnieją w systemie czy nie.  Upraszczając, rozpakowanie oznacza,
że <literal>dpkg</literal> wydobędzie pliki z pliku archiwum, które zostały
przeznaczone do zainstalowania w twoim systemie plików i umieści je we
właściwym miejscu.  Jeśli te pakiety <emphasis>depend</emphasis> istnieją w
kilku innych pakietach w Twoim systemie, <literal>dpkg</literal> odmówi
dokończenia instalacji (przez wykonanie akcji "configure"), dopóki inne
pakiety nie zostaną zainstalowane.
</para>
<para>
Jakkolwiek dla pewnych pakietów <literal>dpkg</literal> będzie odrzucał
nawet ich rozpakowanie, dopóki nie zostaną spełnione pewne zależności .
Takie pakiety są nazywane "Pre-depend" i zależą od obecności innych
pakietów.  System Debian dostarcza ten mechanizm, aby wspierać bezpieczne
aktualizowanie wersji systemów z formatu <literal>a.out</literal> do formatu
<literal>ELF</literal>, gdzie <emphasis>porządek</emphasis> w którym są
rozpakowywane pakiety jest krytycznie ważny.  Są też inne aktualizacje,
gdzie metoda ta jest użyteczna, np.  aktualizacja pakietów z priorytetem
wymagane i ich zależność od LibC.
</para>
<para>
Tak, jak poprzednio, dokładniejsze informacje o tym można znaleźć w
Packaging manual.
</para>
</section>

<section id="pkgstatus"><title>Co oznacza się przez <emphasis>nieznany</emphasis>, <emphasis>instalowany</emphasis>, <emphasis>usunięty</emphasis> <emphasis>wyczyszczony</emphasis> i <emphasis>wstrzymany</emphasis> w statusie pakietu?</title>
<para>
Te flagi mówią, co użytkownik chciał zrobić z pakietem (jako wskazanie
albo przez akcję użytkownika w części "Wybór" programu
<literal>dselect</literal>, lub bezpośrednie wywołanie programu
<literal>dpkg</literal>).
</para>
<para>
Ich znaczenie to:
</para>
<itemizedlist>
<listitem>
<para>
nieznany - użytkownik nigdy nie wskazał co chce zrobić z pakietem.
</para>
</listitem>
<listitem>
<para>
instalowany - użytkownik chciał zainstalować lub zaktualizować wersję
pakietu.
</para>
</listitem>
<listitem>
<para>
usunięty - użytkownik chciał usunąć pakiet, ale nie chciał usuwać
żadnych istniejących plików konfiguracyjnych.
</para>
</listitem>
<listitem>
<para>
wyczyszczony - użytkownik chciał aby pakiet został usunięty całkowicie,
włączając jego pliki konfiguracyjne.
</para>
</listitem>
<listitem>
<para>
wstrzymany - użytkownik nie chce by ten pakiet był przetwarzany, np.  chce
zatrzymać obecną wersję z obecnym statusem bez względu na to, jaki on jest.
</para>
</listitem>
</itemizedlist>
</section>

<section id="puttingonhold"><title>Jak można dokonać zatrzymania pakietu?</title>
<para>
Są dwie drogi zatrzymania pakietów, używając dpkg lub dselect.
</para>
<para>
Używając dpkg możesz wyeksportować listę wyborów pakietów poleceniem:
</para>
<screen>
dpkg --get-selections \* > selections.txt
</screen>
<para>
Potem wyedytować wynikowy plik <filename>selections.txt</filename>,
zmieniając linię zawierającą pakiet, który chcesz zatrzymać, np.
<systemitem role="package">libc6</systemitem>, z tego:
</para>
<screen>
libc6                                           install
</screen>
<para>
na to:
</para>
<screen>
libc6                                           hold
</screen>
<para>
Zachować plik i wczytać to do bazy dpkg poleceniem:
</para>
<screen>
dpkg --set-selections < selections.txt
</screen>
<para>
Używając dselect wystarczy wybrać ekran [S]elect, znaleźć pakiet, który
chcesz zatrzymać w obecnym stanie i nacisnąć klawisz `=' (lub `H').  Zmiany
wejdą w życie natychmiast po wyjściu z ekranu [S]elect.
</para>
</section>

<section id="sourcepkgs"><title>Jak mogę zainstalować pakiet źródłowy?</title>
<para>
Pakiety źródłowe systemu Debian nie mogą być aktualnie "zainstalowane",
są one po prostu rozpakowywane do katalogu, w którym chcesz zbudować pakiety
binarne przez nie tworzone.
</para>
<para>
Pakiety źródłowe są rozpowszechniane w większości na tych samych
serwerach lustrzanych, na których możesz uzyskać pakiety binarne.  Jeśli
ustawisz swoje APT
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
aby zawierało stosowne linie "deb-src", będziesz zdolny łatwo ściągnąć
każdy pakiet źródłowy uruchamiając
</para>
<screen>
apt-get source foo
</screen>
<para>
Aby pomóc w tworzeniu aktualnego pakietu, Pakiet źródłowy systemu Debian
dostarcza tak zwanego mechanizmu tworzenia zależności.  Oznacza to, że
opiekun pakietu źródłowego utrzymuje listę innych pakietów, które są
wymagane aby stworzyć pakiet.  Aby zobaczyć jak jest to użyteczne uruchom
</para>
<screen>
apt-get build-dep foo
</screen>
<para>
przed tworzeniem pakietu.
</para>
</section>

<section id="sourcebuild"><title>Jak się buduje binarne pakiety z pakietu źródłowego?</title>
<para>
Aby skompilować źródła, będziesz potrzebował wszystkie pliki typu
foo_*.dsc, foo_*.tar.gz i foo_*.diff.gz (zauważ: czasami nie ma plików
.diff.gz dla pakietów, które są rodzime dla systemu Debian).
</para>
<para>
Gdy masz pakiety (<xref linkend="sourcepkgs"/>) i jeśli masz pakiet
<systemitem role="package">dpkg-dev</systemitem> zainstalowany, następujące
polecenia:
</para>
<screen>
dpkg-source -x foo_version-revision.dsc
</screen>
<para>
rozpakuje pakiet do katalogu nazwanego <literal>foo-version</literal>.
</para>
<para>
Jeśli chcesz tylko skompilować pakiet, możesz przejść do katalogu
<literal>foo-version</literal> i wydać polecenie
</para>
<screen>
dpkg-buildpackage -rfakeroot -b
</screen>
<para>
aby zbudować pakiet (zauważ że to także wymaga pakietu <systemitem
role="package">fakeroot</systemitem> ) i potem
</para>
<screen>
dpkg -i ../foo_version-revision_arch.deb
</screen>
<para>
aby zainstalować nowo-zbudowany pakiet.
</para>
</section>

<section id="creatingdebs"><title>Jak mogę stworzyć pakiet systemu Debian samodzielnie?</title>
<para>
Bardziej dokładnego opisu o tym szukaj w Instrukcji dla nowych opiekunów,
dostępnej w pakiecie <systemitem role="package">maint-guide</systemitem> lub
na stronie <ulink
url="http://www.debian.org/doc/devel-manuals#maint-guide">http://www.debian.org/doc/devel-manuals#maint-guide</ulink>.
</para>
</section>

</chapter>

