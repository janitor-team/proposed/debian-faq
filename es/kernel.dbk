<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="kernel"><title>Debian y el núcleo</title>
<section id="customkernel"><title>¿Qué herramientas proporciona Debian para crear núcleos personalizados?</title>
<para>
Se recomienda a todos aquellos usuarios que deseen (o necesiten) compilar un
núcleo personalizado que obtengan el paquete <literal>kernel-package</literal>
(que se encuentra en la sección <literal>misc</literal> en los sitios FTP de
Debian).  Este paquete contiene el script necesario para compilar el núcleo, y
permite crear un paquete Debian kernel-image sólo con la ejecución de la
orden <literal>make-kpkg kernel_image</literal> en el directorio principal de
los fuentes del núcleo.  Hay disponible una ayuda ejecutando la orden
<literal>make-kpkg --help</literal>, y en la página manual de make-kpkg(1).
</para>
<para>
Los usuarios deberán obtener separadamente el código fuente del último
núcleo (o del núcleo que quieran) de su servidor de Linux favorito.
</para>
<para>
Para construir un núcleo personalizado, se deberán tener los siguientes
paquetes instalados: <literal>gcc</literal>, <literal>libc6-dev</literal>,
<literal>bin86</literal>, <literal>binutils</literal>, y
<literal>make</literal>.
</para>
<para>
En el fichero <filename>/usr/share/doc/kernel-package/README.gz</filename> se
dan instrucciones detalladas del uso del paquete.  Resumidamente se debe:
</para>
<itemizedlist>
<listitem>
<para>
Descomprimir los fuentes del núcleo, y hacer un <literal>cd</literal> al
directorio recién creado.
</para>
</listitem>
<listitem>
<para>
Modificar la configuración del núcleo usando una de las siguientes órdenes:
</para>
<itemizedlist>
<listitem>
<para>
<literal>make config</literal> (para terminales, con un interfaz
línea-a-línea).
</para>
</listitem>
<listitem>
<para>
<literal>make menuconfig</literal> (para un interfaz por menús basado en
ncurses).  Observe que para usar esta opción debe estar instalado el paquete
<literal>libncurses5-dev</literal>.
</para>
</listitem>
<listitem>
<para>
<literal>make xconfig</literal> (para un interfaz X11).  Para usar esta opción
se requiere tener instalados los paquetes relevantes de X.
</para>
</listitem>
</itemizedlist>
<para>
Cualquiera de los pasos anteriores genera un nuevo <literal>.config</literal>
en el directorio principal de los fuentes del núcleo.
</para>
</listitem>
<listitem>
<para>
Ejecutar la orden: <literal>make-kpkg -rev Custom.N kernel_image</literal>,
donde N es un número de revisión asignado por el usuario.  El nuevo archivo
Debian así creado tendrá la revisión Custom.N, por ejemplo,
<literal>kernel-image-2.4.27_Custom.1_i386.deb</literal> para el núcleo
2.4.27.
</para>
</listitem>
<listitem>
<para>
Instalar el paquete creado.
</para>
<itemizedlist>
<listitem>
<para>
<literal>Ejecute dpkg --install
/usr/src/kernel-image-VVV_Custom.N.deb</literal> para instalar el núcleo.  El
script de instalación,
</para>
<itemizedlist>
<listitem>
<para>
ejecutará el cargador de arranque, LILO (si está instalado),
</para>
</listitem>
<listitem>
<para>
instalará el núcleo personalizado en /boot/vmlinuz_VVV-Custom.N, y
establecerá los enlaces simbólicos apropiados a la nueva versión del
núcleo.
</para>
</listitem>
<listitem>
<para>
preguntará al usuario si quiere hacer un disquete de arranque.  Este disco de
arranque contendrá sólo el núcleo.  Mire las notas adicionales sobre la
creación de un <xref linkend="cusboot"/>.
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Para usar otros cargadores de arranque (como <literal>loadlin</literal>), puede
copiar esta imagen a otros lugares (por ejemplo, a una partición
<literal>MS-DOS</literal>).
</para>
</listitem>
</itemizedlist>
</listitem>
</itemizedlist>
</section>

<section id="cusboot"><title>¿Cómo puedo crear un disco de arranque personalizado?</title>
<para>
Esta tarea se facilita mucho con el paquete <literal>boot-floppies</literal>,
que se encuentra normalmente en la sección <literal>admin</literal> de los
archivos FTP de Debian.  Los scripts de este paquete producen discos de
arranque en el formato <literal>SYSLINUX</literal>.  Son discos con formato
<literal>MS-DOS</literal> cuyo master boot record (registro maestro de
arranque) ha sido alterado para que arranque Linux (o cualquier otro sistema
operativo que se haya definido en el fichero syslinux.cfg en el disquete)
directamente.  Otros scripts de este paquete sirven para producir discos root
de emergencia y pueden incluso reproducir los discos base.  Encontrará más
información acerca de esto en el fichero
<literal>/usr/share/doc/boot-floppies/README</literal> después de instalar el
paquete <literal>boot-floppies</literal>.
</para>
</section>

<section id="modules"><title>¿Qué medios especiales proporciona Debian para el uso de módulos?</title>
<para>
El paquete <literal>modconf</literal> de Debian proporciona un shell script
(<literal>/usr/sbin/modconf</literal>) que puede usarse para personalizar la
configuración de los módulos.  Este script presenta un interfaz basado en
menús, preguntando al usuario acerca de las particularidades de los
controladores de dispositivos de su sistema.  Las respuestas se utilizan para
personalizar el fichero <literal>/etc/conf.modules</literal> (que lista alias y
otros argumentos que deben usarse conjuntamente con algunos módulos), y
<literal>/etc/modules</literal> (que lista los módulos que deben cargarse en
el arranque).  Al igual que los (nuevos) ficheros Configure.help que hay ahora
disponibles para ayudar en la construcción de núcleos personalizados, el
paquete modconf viene con una serie de ficheros de ayuda (en
<literal>/usr/lib/modules_help/</literal>) que proporcionan información
detallada sobre los argumentos apropiados para cada uno de los módulos.
</para>
</section>

<section id="removeoldkernel"><title>¿Puedo desinstalar sin peligro un núcleo antiguo y, si es así, cómo?</title>
<para>
Sí.  El script <literal>kernel-image-NNN.prerm</literal> comprueba antes si el
núcleo que está utilizando actualmente es el mismo que quiere desinstalar.
Por lo tanto puede borrar paquetes de núcleos no deseados ejecutando la
siguiente orden:
</para>
<screen>
dpkg --purge --force-remove-essential kernel-image-NNN
</screen>
</section>

</chapter>

