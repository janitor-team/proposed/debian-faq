<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="support"><title>Cómo obtener soporte para Debian GNU/Linux</title>
<section id="debiandocs"><title>¿Qué otra documentación existe sobre y para un sistema Debian?</title>
<itemizedlist>
<listitem>
<para>
Instrucciones de instalación para la versión actual: ver <ulink
url="http://www.debian.org/releases/stable/installmanual">http://www.debian.org/releases/stable/installmanual</ulink>.
</para>
</listitem>
<listitem>
<para>
El manual de normas de Debian documenta los requisitos de la distribución, es
decir, la estructura y contenido de los archivos de Debian, varias cuestiones
de diseño del sistema operativo, etc.  También incluye los requisitos
técnicos que deben satisfacer los paquetes para que sean incluidos en la
distribución, y documenta los aspectos técnicos básicos de los paquetes
Debian binarios y fuentes.
</para>
<para>
Se puede obtener del paquete <systemitem
role="package">debian-policy</systemitem>, o en <ulink
url="http://www.debian.org/doc/devel-manuals#policy">http://www.debian.org/doc/devel-manuals#policy</ulink>.
</para>
</listitem>
<listitem>
<para>
Documentación sobre paquetes Debian instalados: La mayoría de los paquetes
tienen ficheros que son desempaquetados en
<literal>/usr/share/doc/PAQUETE</literal>.
</para>
</listitem>
<listitem>
<para>
Documentación sobre el proyecto Linux: El paquete Debian <systemitem
role="package">doc-linux</systemitem> instala las versiones más recientes de
todos los HOWTOs y mini-HOWTOs procedentes del <ulink
url="http://www.tldp.org/">Linux Documentation Project</ulink>.
</para>
</listitem>
<listitem>
<para>
Páginas 'man' estilo Unix: La mayoría de las órdenes tienen páginas de
manual escritas en el estilo de los ficheros 'man' originales de Unix.  Están
referenciados por la sección del directorio 'man' donde residen: p.ej., fu(3)
se refiere a una página de manual que reside en /usr/share/man/man3/, y puede
ser llamada usando la orden: <literal>man 3 fu</literal>, o simplemente
<literal>man fu</literal> si la sección 3 es la única que contiene una
página sobre <literal>fu</literal>.
</para>
<para>
Se puede saber qué directorio de <literal>/usr/share/man/</literal> contiene
una cierta página de manual ejecutando <literal>man -w fu</literal>.
</para>
<para>
Los nuevos usuarios de Debian deben tener en cuenta que las páginas 'man' de
muchas órdenes generales del sistema no estarán disponibles hasta que
instalen estos paquetes:
</para>
<itemizedlist>
<listitem>
<para>
<literal>man-db</literal>, que contiene el propio programa
<literal>man</literal>, así como otros programas para manipular las páginas
de manual.
</para>
</listitem>
<listitem>
<para>
<literal>manpages</literal>, que contiene las páginas de manual del
sistema(ver <xref linkend="nonenglish"/>).
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Páginas 'info' estilo GNU: La documentación de usuario de muchas órdenes,
particularmente herramientas GNU, no está disponible en páginas 'man', sino
en ficheros 'info' que se pueden leer con la herramienta GNU
<literal>info</literal>, ejecutando <literal>M-x info</literal> desde GNU
Emacs, o con cualquier otro visor de páginas Info.  Su principal ventaja sobre
las páginas 'man' originales es que componen un sistema hipertexto.
<emphasis>No</emphasis> requiere la WWW; <literal>info</literal> puede
ejecutarse desde una consola de texto.  Fue diseñado por Richard Stallman y
precedió la WWW.
</para>
</listitem>
</itemizedlist>
<para>
También se puede acceder a un montón de documentación del sistema utilizando
un navegador Web, a través de las órdenes `dwww' o `dhelp', que se encuentran
en sus paquetes respectivos.
</para>
</section>

<section id="onlineresources"><title>¿Existe algún recurso en línea para hablar sobre Debian?</title>
<para>
Sí.  De hecho, el método principal de soporte al usuario que proporciona
Debian es por medio del correo electrónico.
</para>
<section id="s11.2.1"><title>Listas de correo</title>
<para>
Hay un montón de <ulink url="http://www.debian.org/MailingLists/">Listas de
correo sobre Debian</ulink>.
</para>
<para>
En un sistema que tenga instalado el paquete <systemitem
role="package">doc-debian</systemitem> hay una lista completa de las listas de
correo en <filename>/usr/share/doc/debian/mailing-lists.txt</filename>.
</para>
<para>
Las listas de correo de Debian tienen nombres de la forma
debian-<replaceable>tema-de-la-lista</replaceable>, por ejemplo,
debian-announce, debian-user, debian-news.  Para suscribirse a cualquier lista
debian-<replaceable>tema-de-la-lista</replaceable>, envía un mensaje a
debian-<replaceable>tema-de-la-lista</replaceable>-request@lists.debian.org con
la palabra "subscribe" en la cabecera Subject:.  Asegúrese de añadir
<emphasis>-request</emphasis> a la dirección de correo cuando utilice este
método para suscribirse o darse de baja, porque de lo contrario el mensaje
irá a la propia lista, lo cual puede ser embarazoso o desconcertante,
dependiendo de su punto de vista.
</para>
<para>
Si tiene un navegador de Web, puede suscribirse a las listas de correo
utilizando el <ulink
url="http://www.debian.org/MailingLists/subscribe">Formulario WEB</ulink>.
También puede darse de baja utilizando un <ulink
url="http://www.debian.org/MailingLists/unsubscribe">Formulario WEB</ulink>.
</para>
<para>
La dirección de correo del gestor de la lista es
<email>listmaster@lists.debian.org</email>, en caso de que tenga cualquier
problema.
</para>
<para>
Los archivos de las lisats de correo de Debian están disponibles a través de
la WEB en <ulink
url="http://lists.debian.org/">http://lists.debian.org/</ulink>.
</para>
<section id="mailinglistconduct"><title>¿Cuál es el código de conducta en las listas de correo?</title>
<para>
Cuando use una de las listas de correo de Debian, por favor siga estas reglas:
</para>
<itemizedlist>
<listitem>
<para>
No envíe spam.
</para>
</listitem>
<listitem>
<para>
No ofenda; no es cortés.  Además, la gente que está desarrollando Debian son
todos voluntarios que donan su tiempo, energía y dinero en un intento de
agrupar los esfuerzos del proyecto Debian.
</para>
</listitem>
<listitem>
<para>
No use lenguaje obsceno; hay quien recibe las listas via packet radio, donde
las palabras malsonantes son ilegales.
</para>
</listitem>
<listitem>
<para>
Asegúrese de que está usando la lista adecuada.  <emphasis>No</emphasis>
envíe nunca solicitudes de suscripción o de baja a la propia lista
<footnote><para> Utilice la dirección
debian-<replaceable>tema-de-la-lista</replaceable>-REQUEST@lists.debian.org
para ello.  </para> </footnote>
</para>
</listitem>
<listitem>
<para>
Consulte la sección <xref linkend="bugreport"/> sobre cómo comunicar bugs.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="s11.2.2"><title>Mantenedores</title>
<para>
Los usuarios pueden hacer preguntas a los mantenedores de los paquetes por
correo electrónico.  Para ponerse en contacto con el encargado de un paquete
llamado xyz, envíe un mensaje a la dirección
<emphasis>xyz@packages.debian.org</emphasis> También se pueden conocer los
nombres y direcciones de correo de los mantenedores buscándolo en el fichero
"Packages", ya que este fichero es simplemente una concatenación de todos los
ficheros de control de paquete disponibles en un árbol de directorios en
particular.  Para extraer el fichero de control de un paquete Debian en
concreto, use la orden,
</para>
<screen>
dpkg --info packageName_VVV-RRR.deb
</screen>
<para>
Otra lista de correo relacionada, <emphasis
role="strong">debiangame</emphasis>, es mantenida por Gary Moore (<ulink
url="mailto:gary@ssc.com">mailto:gary@ssc.com</ulink>) en la Universidad de
Washington.  Como el nombre sugiere, está dedicada a hablar sobre juegos que
han sido (o serán) empaquetados para Debian.  Para suscribirse, mande un
mensaje a <literal>listproc@u.washington.edu</literal>, poniendo en el cuerpo
del mensaje:
</para>
<screen>
  subscribe debiangame Nombre Apellido
</screen>
<para>
ListProc requiere tanto el Nombre como el Apellido.
</para>
<para>
Los usuarios deberán enviar preguntas no específicas de Debian a uno de los
grupos de noticias de Linux, que se llaman comp.os.linux.* o linux.*.
Specialized Systems Consultants (también conocido como SSC) mantiene una
<ulink url="http://www.ssc.com/linux/news.html">lista de grupos de noticias
sobre Linux, Unix, X, y redes</ulink> en su sitio WWW.
</para>
</section>

</section>

<section id="searchtools"><title>¿Hay alguna forma rápida de buscar información sobre Debian GNU/Linux?</title>
<para>
Hay toda una variedad de motores de búsqueda que sirven documentación
relacionada con Debian.
</para>
<itemizedlist>
<listitem>
<para>
<ulink url="http://insite.verisim.com/search/debian/simple">Verism's search
site</ulink>.  Para obtener información sobre cómo remitir un informe de bug,
introduzca las palabras <literal>debian bug submit</literal> y busque por "all
of these words".
</para>
</listitem>
<listitem>
<para>
<ulink url="http://www.dejanews.com/">DejaNews news search service</ulink>.
Para encontrar las experiencias que la gente ha tenido en la búsqueda de
manejadores para las controladoras Western Digital, pruebe la búsqueda de esta
frase:
</para>
<screen>
linux & WD
</screen>
<para>
Esto le dice a DejaNews que dé cuenta de cualquier mensaje conteniendo la
cadena "linux" Y la cadena "WD".  Cuando lo usé, descubrí que mi tarjeta WD
(la cual tenía desde hacía solo 6 meses) ha sido declarada obsoleta por
Adaptec, ahora que han comprado WD.  Así que no hay manejadores disponibles.
(Bendito sea Adaptec.)
</para>
</listitem>
<listitem>
<para>
El <ulink url="http://altavista.digital.com/">AltaVista Search Engine</ulink>
también se puede usar para hacer búsquedas en Usenet (aunque parece que no
está tan al día como DejaNews).  Por ejemplo, buscar la cadena "cgi-perl" da
una explicación más detallada de este paquete que el breve campo de
descripción de su fichero de control.
</para>
</listitem>
</itemizedlist>
</section>

<section id="buglogs"><title>¿Existen registros de bugs conocidos?</title>
<para>
La distribución Debian GNU/Linux tiene un sistema de seguimiento de bugs (BTS)
que almacena detalles de bugs notificados por usuarios y desarrolladores.  Se
asigna un número a cada bug y se mantiene almacenado hasta que se marca como
resuelto.
</para>
<para>
Esta información se encuentra en <ulink
url="http://www.debian.org/Bugs/">http://www.debian.org/Bugs/</ulink>.
</para>
<para>
Un servidor de correo proporciona acceso a través de correo electrónico a la
base de datos del sistema de seguimiento.  Para obtener las instrucciones
envíe un mensaje a request@bugs.debian.org con la palabra "help" en el cuerpo
del mensaje.
</para>
</section>

<section id="bugreport"><title>¿Cómo informo de un bug en Debian?</title>
<para>
Si ha encontrado un bug en Debian, por favor lea las instrucciones para
informar de un bug en Debian.  Estas instrucciones pueden obtenerse de
cualquiera de las siguientes formas:
</para>
<itemizedlist>
<listitem>
<para>
Por FTP anónimo.  Los sitios réplica de Debian contienen las instrucciones en
el fichero <literal>doc/bug-reporting.txt</literal>.
</para>
</listitem>
<listitem>
<para>
Desde la WWW.  Hay una copia de las instrucciones en <ulink
url="http://www.debian.org/Bugs/Reporting.html">http://www.debian.org/Bugs/Reporting.html</ulink>
</para>
</listitem>
<listitem>
<para>
En cualquier sistema Debian con el paquete <literal>doc-debian</literal>
instalado.  Las instrucciones se encuentran en el fichero <literal><ulink
url="file:/usr/share/doc/debian/bug-reporting.txt">/usr/share/doc/debian/bug-reporting.txt</ulink></literal>.
</para>
</listitem>
</itemizedlist>
<para>
Use estas direcciones de correo para enviar informes de bugs:
</para>
<itemizedlist>
<listitem>
<para>
submit@bugs.debian.org: para informes de bugs generales.  Deberá recibir una
confirmación automática de su notificación.  También se le asignará
automáticamente un número de seguimiento de bug, entrará en el registro de
bugs y será enviado a la lista de correo debian-bugs-dist.
</para>
</listitem>
<listitem>
<para>
maintonly@bugs.debian.org: para enviar informes de bugs sólo al que mantiene
el paquete.  No será enviado a la lista de correo debian-bugs-dist.
</para>
</listitem>
<listitem>
<para>
quiet@bugs.debian.org: para enviar informes de bugs sólo al registro de bugs,
sin enviarlos a debian-bugs-dist o al que mantiene el paquete.
</para>
</listitem>
</itemizedlist>
<para>
Por favor, tenga en cuenta que existe un comprobador de paquetes Debian llamado
<ulink url="http://www.debian.org/lintian/">Lintian</ulink>, que está
diseñado para comprobar de forma automática violaciones de las normas y otros
errores de empaquetado comunes de los paquetes Debian.  De esta forma, si usted
detecta un bug en un paquete que sea probable que aparezca también en otros
paquetes, podría ser mejor ponerse en contacto con los <ulink
url="mailto:lintian-maint@debian.org">encargados de Lintian</ulink> de forma
que se escriba una nueva comprobación para Lintian en lugar de informar del
bug directamente.  Esto evitará que el bug aparezca de nuevo en versiones
posteriores del paquete, o de cualquier otro paquete de la distribución.
</para>
</section>

</chapter>

