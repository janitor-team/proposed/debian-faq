<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="customizing"><title>Personalizando sua instalação do Debian GNU/Linux.</title>
<section id="papersize"><title>Como posso garantir que todos os programas usem o mesmo tamanho de papel?</title>
<para>
O arquivo <literal>/etc/papersize</literal> contém o nome do tamanho padrão
do papel (como carta ou A4).  Ele pode ser sobrescrito usando a variável de
ambiente <literal>PAPERSIZE</literal>.  Para detalhes, veja a página de manual
<literal>papersize(5)</literal>.
</para>
</section>

<section id="hardwareaccess"><title>Como posso fornecer acesso aos periféricos sem comprometer a segurança?</title>
<para>
Vários arquivos de dispositivo no diretório <literal>/dev</literal> pertencem
a grupos pré-definidos.  Por exemplo, <literal>/dev/fd0</literal> pertence ao
grupo <literal>floppy</literal>, e o <literal>/dev/dsp</literal> pertence ao
grupo <literal>audio</literal>.
</para>
<para>
Se você deseja que um certo usuário tenha acesso a um desses dispositivos,
somente adicione o usuário ao grupo ao qual o dispositivo pertence, por
exemplo, faça:
</para>
<screen>
adduser user group
</screen>
<para>
Desse modo, você não terá que usar <command>chmod</command> no arquivo de
dispositivo.
</para>
</section>

<section id="consolefont"><title>Como carrego uma fonte de console na inicialização à moda Debian?</title>
<para>
O pacote <systemitem role="package">kbd</systemitem> e <systemitem
role="package">console-tools</systemitem> suportam isso, edite o arquivo
<literal>/etc/kbd/config</literal> ou o arquivo
<literal>/etc/console-tools/config</literal>.
</para>
</section>

<section id="appdefaults"><title>Como configuro os defaults de um programa para X11?</title>
<para>
Os programas X do Debian irão instalar seus dados de recursos para os
aplicativos em <literal>/etc/X11/app-defaults/</literal>.  Se quiser
personalizar aplicativos do X globalmente, coloque suas personalizações
nesses arquivos.  Eles são marcados como arquivos de configuração, portanto,
seus conteúdos serão preservados durante atualizações.
</para>
</section>

<section id="booting"><title>Parece que cada distribuição possui um método de inicialização diferente. Fale-me sobre o do Debian.</title>
<para>
Como todos os Unices, o Debian é inicializado executando-se o programa
<literal>init</literal>.  O arquivo de configuração para o
<literal>init</literal> (que é o <literal>/etc/inittab</literal>) especifica
que o primeiro script a ser executado deve ser o
<literal>/etc/init.d/rcS</literal>.  Este script verifica e monta sistemas de
arquivos, carrega módulos, inicia os serviços de rede (chamando o script
<literal>/etc/init.d/network</literal>), ajusta o relógio, faz outras
inicializações, e daí, executa todos os scripts (exceto os que tiverem '.'
no nome-de-arquivo) em <literal>/etc/rc.boot/</literal>.  Qualquer script no
diretório anterior é normalmente reservado para uso do administrador do
sistema, e o uso deles em pacotes é desaprovado.
</para>
<para>
Após completar o processo de inicialização, o <literal>init</literal>
executa todos os scripts "start" do diretório especificado pelo runlevel
padrão (este runlevel é dado pela entrada <literal>id</literal> no
<literal>/etc/inittab</literal>).  Como a maioria dos Unices compatíveis com o
System V, o Linux possui 7 runlevels:
</para>
<itemizedlist>
<listitem>
<para>
0 (pára o sistema),
</para>
</listitem>
<listitem>
<para>
1 (modo monousuário),
</para>
</listitem>
<listitem>
<para>
2 até 5 (vários modos multiusuários), e
</para>
</listitem>
<listitem>
<para>
6 (reinicializa o sistema).
</para>
</listitem>
</itemizedlist>
<para>
Sistemas Debian vêm com id=2, que indica que o runlevel padrão será o '2'
quando o sistema entrar no modo multiusuário, e os scripts de
<literal>/etc/rc2.d/</literal> serão executados.
</para>
<para>
Na verdade, os scripts em quaisquer dos diretórios
<literal>/etc/rcN.d/</literal> são apenas ligações simbólicas para scripts
em <literal>/etc/init.d/</literal>.  Porém, os <emphasis>nomes</emphasis> dos
arquivos em cada um dos diretórios <literal>/etc/rcN.d/</literal> são
escolhidos de modo a indicar a <emphasis>maneira</emphasis> em que os scripts
de <literal>/etc/init.d/</literal> serão executados.  Especificamente, antes
de entrar em qualquer runlevel, todos os scripts que começam com a letra 'K'
são executados; esses scripts "matam" (terminam) os serviços.  Depois, todos
os scripts que começam com a letra 'S' são executados; esses scripts iniciam
serviços.  O número de dois dígitos após o 'K' ou 'S' indica a ordem na
qual o script é executado.  Os scripts com números menores são executados
primeiro.
</para>
<para>
Esta abordagem funciona porque todos os scripts em
<literal>/etc/init.d/</literal> aceitam um argumento que pode ser tanto 'start'
(iniciar), 'stop' (parar), ou 'reload' (recarregar), 'restart' (reiniciar) ou
'force-reload' (força/obriga a recarga) e fazem a tarefa indicada pelo
argumento.  Esses scripts podem ser usados mesmo depois que o sistema tiver
sido inicializado, para controlar os vários processos.
</para>
<para>
Por exemplo, com o argumento `reload' o comando
</para>
<screen>
/etc/init.d/sendmail reload
</screen>
<para>
envia ao daemon do sendmail um sinal para reler seu arquivo de configuração.
</para>
</section>

<section id="custombootscripts"><title>Parece que o Debian não usa o arquivo <literal>rc.local</literal> para personalizar o processo de inicialização; que recursos são fornecidos?</title>
<para>
Suponha que um sistema precisa executar o script <literal>foo</literal> na
inicialização, ou uma entrada para um runlevel (System V) específico.
Então, o administrador do sistema deve:
</para>
<itemizedlist>
<listitem>
<para>
Colocar o script <literal>foo</literal> no diretório
<literal>/etc/init.d/</literal>.
</para>
</listitem>
<listitem>
<para>
Executar o comando <literal>update-rc.d</literal> do Debian com os argumentos
apropriados, para estabelecer as ligações entre os diretórios rc?.d e
<literal>/etc/init.d/foo</literal> (especificados na linha de comando).  Aqui,
'?'  é um número de 0 a 6 e corresponde a cada um dos runlevels do System V.
</para>
</listitem>
<listitem>
<para>
Reinicializar o sistema.
</para>
</listitem>
</itemizedlist>
<para>
O comando <literal>update-rc.d</literal> estabelecerá as ligações entre os
arquivos dos diretórios rc?.d e o script em <literal>/etc/init.d/</literal>.
Cada ligação começará com um 'S' ou um 'K', seguido de um número, seguido
do nome do script.  Os scripts de <literal>/etc/rcN.d/</literal> que começam
com a letra 'S' são executados quando se entra no runlevel
<literal>N</literal>.  Os scripts que começam com a letra 'K' são executados
quando se deixa o runlevel <literal>N</literal>.
</para>
<para>
Pode-se, por exemplo, fazer com que o script <literal>foo</literal> seja
executado na inicialização, colocando-o em <literal>/etc/init.d/</literal> e
instalando as ligações com <literal>update-rc.d foo defaults 19</literal>.  O
argumento 'defaults' refere-se aos runlevels padrões, que vão de 2 a 5.  O
argumento '19' assegura que <literal>foo</literal> será chamado antes de
quaisquer scripts que tenham números iguais ou maiores a 20.
</para>
</section>

<section id="interconffiles"><title>Como o sistema de gerenciamento de pacotes lida com pacotes que contêm arquivos de configuração para outros pacotes?</title>
<para>
Alguns usuários gostariam de criar, por exemplo, um novo servidor instalando
um grupo de pacotes Debian e um pacote gerado localmente que consiste em
arquivos de configuração.  Isso geralmente não é uma boa idéia, porque o
<command>dpkg</command> não saberá que esses arquivos de configuração
existem se eles estiverem num pacote diferente, e pode escrever configurações
conflitantes quando um pacote do "grupo" inicial for atualizado.
</para>
<para>
Ao invés disso, crie um pacote local que modifica os arquivos de
configuração do "grupo" de pacotes Debian em questão.  Daí, o
<command>dpkg</command> e o resto do sistema de gerenciamento de pacotes verá
que os arquivos foram modificados pelo "administrador" local e não tentará
sobrescrevê-los quando esses pacotes forem atualizados.
</para>
</section>

<section id="divert"><title>Como posso anulo um arquivo instalado por um pacote, de modo que uma versão diferente possa ser usada?</title>
<para>
Suponha que um administrador ou um usuário local queira usar um programa
"login-local" ao invés do programa "login" fornecido pelo pacote Debian
<systemitem role="package">login</systemitem>.
</para>
<para>
<emphasis role="strong">Não</emphasis> faça:
</para>
<itemizedlist>
<listitem>
<para>
Sobrescreva <literal>/bin/login</literal> com <literal>login-local</literal>.
</para>
</listitem>
</itemizedlist>
<para>
O sistema de gerenciamento de pacotes não saberá sobre esta mudança, e
simplesmente sobrescreverá seu <literal>/bin/login</literal> personalizado
quando <literal>login</literal> (ou qualquer pacote que forneça o
<literal>/bin/login</literal>) seja instalado ou atualizado.
</para>
<para>
Ao invés disso, faça
</para>
<itemizedlist>
<listitem>
<para>
Execute:
</para>
<screen>
dpkg-divert --divert /bin/login.debian /bin/login
</screen>
<para>
para que todas as futuras instalações de pacotes <systemitem
role="package">login</systemitem> do Debian escrevam o arquivo
<literal>/bin/login</literal> com o nome <literal>/bin/login.debian</literal>.
</para>
</listitem>
<listitem>
<para>
Então execute:
</para>
<screen>
cp login-local /bin/login
</screen>
<para>
para mover seu programa construído localmente ao local correto.
</para>
</listitem>
</itemizedlist>
<para>
Detalhes são dados na página de manual
<citerefentry><refentrytitle>dpkg-divert</refentrytitle><manvolnum>8</manvolnum></citerefentry>.
</para>
</section>

<section id="localpackages"><title>Como posso incluir meu pacote feito localmente, na lista de pacotes disponíveis que o sistema de gerenciamento de pacotes usa?</title>
<para>
Execute o comando:
</para>
<screen>
dpkg-scanpackages BIN_DIR OVERRIDE_FILE [PATHPREFIX] > my_Packages
</screen>
<para>
onde:
</para>
<itemizedlist>
<listitem>
<para>
BIN-DIR é um diretório onde arquivos de pacotes Debian (que normalmente têm
extensão ".deb") são armazenados.
</para>
</listitem>
<listitem>
<para>
OVERRIDE_FILE é um arquivo que é editado pelos mantenedores da distribuição
e é normalmente armazenado num repositório de FTP da Debian, em
<literal>indices/override.main.gz</literal> para os pacotes Debian na
distribuição "main".  Você pode ignorar isso para arquivos locais.
</para>
</listitem>
<listitem>
<para>
PATHPREFIX é um texto <emphasis>opcional</emphasis> que pode ser prefixado ao
arquivo <literal>Packages.new</literal> sendo produzido.
</para>
</listitem>
</itemizedlist>
<para>
Uma vez que você tenha construído o arquivo <literal>my_Packages</literal>,
conte ao sistema de gerenciamento de pacotes sobre ele, usando o comando:
</para>
<screen>
dpkg --merge-avail my_Packages
</screen>
<para>
Se você está usando o APT, você pode adicionar o repositório local ao seu
arquivo
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>
também.
</para>
</section>

<section id="diverse"><title>Alguns usuários gostam do mawk, outros do gawk; alguns gostam do vim, outros do elvis; alguns gostam do trn, outros do tin; como o Debian lida com diversidade?</title>
<para>
Existem vários casos onde dois pacotes fornecem duas versões diferentes de um
programa, ambos fornecendo a mesma funcionalidade.  Usuários podem preferir
uma ao invés de outra fora de hábito, ou por causa da interface de usuário
de um pacote ser um tanto mais agradável do que a interface de outro.  Outros
usuários do mesmo sistema podem realizar diferentes escolhas.
</para>
<para>
O Debian usa um sistema de pacotes "virtual" que permite aos administradores do
sistema escolher (ou deixar que os usuários escolham) suas ferramentas
favoritas quando houver duas ou mais que forneçam a mesma funcionalidade
básica, e ainda satisfazer requerimentos de dependência de pacotes sem
especificar um pacote específico.
</para>
<para>
Por exemplo, podem existir duas versões diferentes de leitores de news num
sistema.  O pacote do servidor de news pode "recomendar" que haja
<emphasis>algum</emphasis> leitor de news no sistema, mas a escolha entre
<literal>tin</literal> ou <literal>trn</literal> fica a cargo do usuário
individual.  Isto é satisfeito fazendo com que ambos os pacotes
<literal>tin</literal> e <literal>trn</literal> forneçam o pacote virtual
<literal>news-reader</literal>.  O programa a ser chamado é determinado
através de uma ligação que aponta de um arquivo com o nome do pacote virtual
<literal>/etc/alternatives/news-reader</literal> para o arquivo escolhido, por
exemplo, <literal>/usr/bin/trn</literal>.
</para>
<para>
Uma única ligação é insuficiente para dar suporte ao uso total de um
programa alternativo; normalmente, páginas de manual, e possivelmente outros
arquivos de suporte devem ser selecionados também.  O script em Perl
<literal>update-alternatives</literal> fornece uma maneira de assegurar que
todos os arquivos associados a um pacote especificado sejam escolhidos como o
padrão do sistema.
</para>
<para>
Por exemplo, para verificar quais executáveis fornecem o `x-window-manager',
rode:
</para>
<screen>
update-alternatives --display x-window-manager
</screen>
<para>
Se você deseja mudá-lo, rode:
</para>
<screen>
update-alternatives --config x-window-manager
</screen>
<para>
E siga as instruções na tela (basicamente, pressione o número próximo a
entrada que deseja).
</para>
<para>
Se um pacote não se registrou como um gerenciador de janelas por alguma razão
(registre um bug se isso for um erro), ou se você usa um gerenciador de
janelas do diretório /usr/local, as seleções na tela não conterão suas
entradas preferidas.  Você pode atualizar a ligação através das opções de
linha de comando, assim:
</para>
<screen>
update-alternatives --install /usr/bin/x-window-manager \
  x-window-manager /usr/local/bin/wmaker-cvs 50
</screen>
<para>
O primeiro argumento para a opção '--install' é a ligação simbólica que
aponta para /etc/alternatives/NAME, onde NAME é o segundo argumento.  O
terceiro argumento é o programa para o qual /etc/alternatives/NAME deveria
apontar, e o quato argumento é a prioridade (grandes valores significam que a
alternativa será mais provavelmente escolhida automaticamente).
</para>
<para>
Para remover uma alternativa que você adicionou, simplesmente rode:
</para>
<screen>
update-alternatives --remove x-window-manager /usr/local/bin/wmaker-cvs
</screen>
</section>

</chapter>

