<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="contributing"><title>Contribuindo para o projeto Debian.</title>
<para>
Doações de tempo (para desenvolver novos pacotes, manter pacotes existentes,
ou fornecer suporte ao usuário), recursos (para espelhar os repositórios de
FTP e WWW), e dinheiro (para pagar novos equipamentos que possam ser usados em
testes e hardware novo para os repositórios) podem ajudar o projeto.
</para>
<section id="contrib"><title>Como posso me tornar um desenvolvedor de software Debian?</title>
<para>
O desenvolvimento do Debian está aberto a todos, e é preciso que haja novos
usuários com as habilidades adequadas e/ou vontade de aprender para manter
pacotes existentes que foram abandonados por seus mantenedores anteriores, para
desenvolver novos pacotes, e para fornecer suporte ao usuário.
</para>
<para>
Todos os detalhes sobre como se tornar um desenvolvedor Debian podem ser
encontradas no pacote <literal>developers-reference</literal>.  Logo, você
deve instalar esse pacote e o ler atentamente.
</para>
<para>
Uma versão on-line do developers-reference está disponível <ulink
url="http://www.debian.org/doc/packaging-manuals/developers-reference/">em
nosso site web</ulink>.
</para>
</section>

<section id="contribresources"><title>Como posso contribuir com recursos para o projeto Debian?</title>
<para>
Como o projeto pretende fazer um corpo substancial de software rapidamente
facilmente acessível através do globo, espelhos são necessários
urgentemente.  É desejável, mas não absolutamente necessário espelhar todos
os arquivos.  Por favor, visite a página <ulink
url="http://www.debian.org/mirror/size"> tamanho dos espelhos Debian</ulink>
para informações sobre o espaço em disco necessário.
</para>
<para>
A maior parte do espelhamento é feita automaticamente por scripts, sem
intervenção humana.  Entretanto, às vezes acontece uma falha ou mudança no
sistema que requer intervenção humana.
</para>
<para>
Se você tem uma conexão de alta velocidade à Internet, os recursos para
espelhar toda ou parte da distribuição, e está disposto a dedicar seu tempo
(ou encontrar alguém) para fazer uma manutenção regular do sistema, por
favor entre em contato com <email>debian-admin@debian.org</email>.
</para>
</section>

<section id="supportingorganizations"><title>Como posso contribuir financeiramente com o projeto Debian?</title>
<para>
A pessoa pode fazer doações individuais a uma das duas organizações que
são críticas ao desenvolvimento do projeto Debian.
</para>
<section id="SPI"><title>Software in the Public Interest</title>
<para>
Software in the Public Interest (SPI) é uma organização sem fins lucrativos
do tipo IRS 501(c)(3), formada quando a FSF retirou seu patrocínio à Debian.
O propósito da organização é desenvolver e distribuir software livre.
</para>
<para>
Nossos objetivos são bem parecidos com os da FSF, e estimulamos os
programadores a usar a GNU General Public License (Licença Pública Geral GNU)
em seus programas.  Entretanto, nosso foco é ligeiramente diferente, no
sentido de que estamos construindo e distribuindo um sistema Linux que difere
em muitos detalhes técnicos do sistema GNU planejado pela FSF.  Ainda nos
comunicamos com a FSF, e cooperamos mandando para eles alterações nos
softwares GNU e recomendando a nossos usuários fazer doações a FSF e ao
projeto GNU.
</para>
<para>
A SPI pode ser contactada através do endereço: <ulink
url="http://www.spi-inc.org/">http://www.spi-inc.org/</ulink>.
</para>
</section>

<section id="FSF"><title>Free Software Foundation</title>
<para>
Atualmente, não há nenhuma conexão formal entre o Debian e a Free Software
Foundation.  Entretanto, a Free Software Foundation é responsável por alguns
dos componentes de software mais importantes do Debian, incluindo o compilador
C da GNU, o GNU Emacs, e grande parte da biblioteca run-time do C, que é usada
por todos os programas no sistema.  A FSF abriu caminho ao software livre: eles
escreveram a General Public License (Licença Geral Pública), que é usada em
muitos dos programas do Debian, e inventaram o projeto "GNU", para criar um
sistema Unix inteiramente livre.  O Debian deve ser considerado um descendente
direto do sistema GNU.
</para>
<para>
A FSF pode ser contactada através do endereço: <ulink
url="http://www.fsf.org/">http://www.fsf.org/</ulink>.
</para>
</section>

</section>

</chapter>

