<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="ftparchives"><title>The Debian archives</title>
<section id="dists"><title>How many Debian distributions are there?</title>
<para>
There are three major distributions: the "stable" distribution, the "testing"
distribution, and the "unstable" distribution.  The "testing" distribution is
sometimes "frozen" (see <xref linkend="frozen"/>).  Next to these, there is the
"oldstable" distribution (that's just the one from before "stable"), and the
"experimental" distribution.
</para>
<para>
Experimental is used for packages which are still being developed, and with a
high risk of breaking your system.  It's used by developers who'd like to study
and test bleeding edge software.  Users shouldn't be using packages from there,
because they can be dangerous and harmful even for the most experienced people.
</para>
<para>
See <xref linkend="choosing"/> for help when choosing a Debian distribution.
</para>
</section>

<section id="codenames"><title>What are all those names like etch, lenny, etc.?</title>
<para>
They are just "codenames".  When a Debian distribution is in the development
stage, it has no version number but a codename.  The purpose of these codenames
is to make easier the mirroring of the Debian distributions (if a real
directory like <literal>unstable</literal> suddenly changed its name to
<literal>stable</literal>, a lot of stuff would have to be needlessly
downloaded again).
</para>
<para>
Currently, <literal>stable</literal> is a symbolic link to
<literal>&releasename;</literal> (i.e. &debian; &release;) and
<literal>testing</literal> is a symbolic link to <literal>&nextreleasename;</literal>.
This means that <literal>&releasename;</literal> is the current stable distribution
and <literal>&nextreleasename;</literal> is the current testing distribution.
</para>
<para>
<literal>unstable</literal> is a permanent symbolic link to
<literal>sid</literal>, as <literal>sid</literal> is always the unstable
distribution (see <xref linkend="sid"/>).
</para>

<!-- TODO: it might be relevant to merge this section with the next one -->

<section id="oldcodenames"><title>Which other codenames have been used in the past?</title>
<para>
Aside <literal>&releasename;</literal> and <literal>&nextreleasename;</literal>, other codenames
that have been already used are: <literal>buzz</literal> for release 1.1,
<literal>rex</literal> for release 1.2, <literal>bo</literal> for releases
1.3.x, <literal>hamm</literal> for release 2.0, <literal>slink</literal> for
release 2.1, <literal>potato</literal> for release 2.2,
<literal>woody</literal> for release 3.0, <literal>sarge</literal> for release
3.1, <literal>etch</literal> for release 4.0, <literal>lenny</literal> for
release 5.0, <literal>squeeze</literal> for release 6.0,
<literal>wheezy</literal> for release 7, <literal>jessie</literal> for release
8, <literal>stretch</literal> for release 9, <literal>buster</literal> for release 10.
<!-- To be used when bookworm is released: , <literal>bullseye</literal> for release 11. -->
</para>
</section>

<section id="sourceforcodenames"><title>Where do these codenames come from?</title>
<para>
So far they have been characters taken from the "Toy Story" movies by Pixar.
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>buzz</emphasis> (Debian 1.1) was the spaceman Buzz Lightyear,
</para>
</listitem>
<listitem>
<para>
<emphasis>rex</emphasis> (Debian 1.2) was the tyrannosaurus,
</para>
</listitem>
<listitem>
<para>
<emphasis>bo</emphasis> (Debian 1.3) was Bo Peep, the girl who took care of the sheep,
</para>
</listitem>
<listitem>
<para>
<emphasis>hamm</emphasis> (Debian 2.0) was the piggy bank,
</para>
</listitem>
<listitem>
<para>
<emphasis>slink</emphasis> (Debian 2.1) was Slinky Dog, the toy dog,
</para>
</listitem>
<listitem>
<para>
<emphasis>potato</emphasis> (Debian 2.2) was, of course, Mr. Potato,
</para>
</listitem>
<listitem>
<para>
<emphasis>woody</emphasis> (Debian 3.0) was the cowboy,
</para>
</listitem>
<listitem>
<para>
<emphasis>sarge</emphasis> (Debian 3.1) was the sergeant of the Green Plastic Army Men,
</para>
</listitem>
<listitem>
<para>
<emphasis>etch</emphasis> (Debian 4.0) was the toy whiteboard (Etch-a-Sketch),
</para>
</listitem>
<listitem>
<para>
<emphasis>lenny</emphasis> (Debian 5.0) was the toy binoculars,
</para>
</listitem>
<listitem>
<para>
<emphasis>squeeze</emphasis> (Debian 6) was the name of the three-eyed aliens,
</para>
</listitem>
<listitem>
<para>
<emphasis>wheezy</emphasis> (Debian 7) was the rubber toy penguin with a red
bow tie,
</para>
</listitem>
<listitem>
<para>
<emphasis>jessie</emphasis> (Debian 8) was the yodeling cowgirl,
</para>
</listitem>
<listitem>
<para>
<emphasis>stretch</emphasis> (Debian 9) was the rubber toy octopus with suckers
on her eight long arms.
</para>
</listitem>
<listitem>
<para>
<emphasis>buster</emphasis> (Debian 10) was Andy's pet dog.
</para>
</listitem>
<listitem>
<para>
<emphasis>bullseye</emphasis> (Debian 11) was Woody's wooden toyhorse.
</para>
</listitem>
<listitem>
<para>
<emphasis>bookworm</emphasis> (Debian 12) was a green toy worm with a built-in
flashlight who loves reading books.
</para>
</listitem>
<listitem>
<para>
<emphasis>trixie</emphasis> (Debian 13) was a blue plastic triceratops.
</para>
</listitem>
<listitem>
<para>
<emphasis>sid</emphasis> was the evil neighbor kid next door who broke all
toys.
</para>
</listitem>
</itemizedlist>
<para>
The <ulink
url="https://lists.debian.org/debian-devel/1996/06/msg00515.html">decision</ulink>
of using Toy Story names was <ulink
url="https://lists.debian.org/debian-user/1997/04/msg00011.html">made</ulink>
by Bruce Perens who was, at the time, the Debian Project Leader and was working
also at Pixar, the company that produced the movies.
</para>

<!-- TODO (jfs)
  Q: Should we add the trademark info here? Maybe as a footnote
  Mr. Potato is a Registered Trademark of Playskool, Inc.,
    Pawtucket, R.I., a division of Hasbro Inc.
  Slinky Dog is a trademark of Poof Products of Plymouth, Mich.,
  Etch-a-Sketch is a trademark of The Ohio Art Company,
  other characters might also be registered trademarks...
-->

<!-- FOR REFERENCE
  more info of the movies in http://www.pixar.com/featurefilms/ts/
  and  http://www.pixar.com/featurefilms/ts2/
  or
    http://www.imdb.com/title/tt0178952/ for Toy Story 1 (1996)
    http://www.imdb.com/title/tt0395789/ for Toy Story 2 (1999)
    http://www.imdb.com/title/tt0435761/ for Toy Story 3 (2010)
    http://www.imdb.com/title/tt1979376/ for Toy Story 4 (2019)

 Note: we shouldn't put the links in, Pixar needs no additional propaganda
-->

<!--
  More info of the characters in:

  http://en.wikipedia.org/wiki/List_of_Toy_Story_characters
  http://pixar.wikia.com/wiki/Category:Toy_Story_Characters

  characters not used from Toy Story 1 (yet):
    - Andy (the kid)
    - Snake
    - Robot
    - Scud (Sid's dog)
    - Three Eyed Alien
    - Rocky Gibraltar (the wrestling figure)
    - Roller Bob (the remote control car)
    - Legs (one of sid's mutant toys)
    - Hand-in-the-box (one of sid's mutant toys)
    - Duckie (one of sid's mutant toys)
    - Spider Baby (one of sid's mutant toys)
    - Combat Carl (one of sid's mutant toys)
    - Jingle Joe (one of sid's mutant toys)
    - Muscle Duck (Ducky) (one of sid's mutant toys)

  Additional characters from Toy Story 2, also not yet used:
    - Al (the propietor of Al's Toy Farm)
    - Zurg (the Evil Emperor)
    - Hannah (owner of Jessie)
    - Stinky Pete the Prospector (the old fat guy)
    - Mrs. Davis (Andy's Mom)
    - Barbie (the Tour Guide, probably under (c))
    - Mrs. Potato Head
    - Heimlich the Caterpillar
    - Mr. Spell
    - Shark
    - Roly Poly Clown
    - Toddle Tots
    - Mr. Mike (the toy tape recorder)
    - Troikas
    - Barrel of Monkeys
    - Hockey Puck 

  From Toy Story 3, not yet used:
    - Chuckles, the brokenhearted clown
    - Dolly
    - Mr. Pricklepants
    - Trixie
    - Buttercup
    - Peas-in-a-Pod
    - Totoro
    - Lots-O'-Huggin' Bear (Lotso)
    - Ken
    - Big Baby
    - Twitch
    - Chunk
    - Sparks
    - Chatter Telephone
    - Cymbal-banging monkey
-->

<!-- (jfs) Just in case somebody misses the "What do we do when we finish
with Toy Story characters" thread see:
http://lists.debian.org/debian-devel/2002/debian-devel-200207/msg01133.html

I suggested we followed with either "Monster's Inc" or "A Bug's life" :)
-->


</section>

</section>

<section id="sid"><title>What about "sid"?</title>
<para>
<emphasis>sid</emphasis> or <emphasis>unstable</emphasis> is the place where
most of the packages are initially uploaded.  It will never be released
directly, because packages which are to be released will first have to be
included in <emphasis>testing</emphasis>, in order to be released in
<emphasis>stable</emphasis> later on.  sid contains packages for both released
and unreleased architectures.
</para>
<para>
The name "sid" also comes from the "Toy Story" animated motion picture: Sid was
the boy next door who destroyed toys :-)
</para>
<para>
<footnote><para> When the present-day sid did not exist, the FTP site
organization had one major flaw: there was an assumption that when an
architecture is created in the current unstable, it will be released when that
distribution becomes the new stable.  For many architectures that isn't the
case, with the result that those directories had to be moved at release time.
This was impractical because the move would chew up lots of bandwidth.  </para>
<para> The archive administrators worked around this problem for several years
by placing binaries for unreleased architectures in a special directory called
"sid".  For those architectures not yet released, the first time they were
released there was a link from the current stable to sid, and from then on they
were created inside the unstable tree as normal.  This layout was somewhat
confusing to users.  </para> <para> With the advent of package pools (see <xref
linkend="pools"/>), binary packages began to be stored in a canonical location
in the pool, regardless of the distribution, so releasing a distribution no
longer causes large bandwidth consumption on the mirrors (there is, however, a
lot of gradual bandwidth consumption throughout the development process).
</para> </footnote>
</para>
</section>

<section id="stable"><title>What does the stable directory contain?</title>
<itemizedlist>
<listitem>
<para>
stable/main/: This directory contains the packages which formally constitute
the most recent release of the &debian; system.
</para>
<para>
These packages all comply with the <ulink
url="https://www.debian.org/social_contract#guidelines">Debian Free Software
Guidelines</ulink>, and are all freely usable and distributable.
</para>
</listitem>
<listitem>
<para>
stable/non-free/: This directory contains packages distribution of which is
restricted in a way that requires that distributors take careful account of the
specified copyright requirements.
</para>
<para>
For example, some packages have licenses which prohibit commercial
distribution.  Others can be redistributed but are in fact shareware and not
free software.  The licenses of each of these packages must be studied, and
possibly negotiated, before the packages are included in any redistribution
(e.g., in a CD-ROM).
</para>
</listitem>
<listitem>
<para>
stable/contrib/: This directory contains packages which are DFSG-free and
<emphasis>freely distributable</emphasis> themselves, but somehow depend on a
package that is <emphasis>not</emphasis> freely distributable and thus
available only in the non-free section.
</para>
</listitem>
</itemizedlist>
</section>

<section id="testing"><title>What does the testing distribution contain?</title>
<para>
Packages are installed into the "testing" directory after they have undergone
some degree of testing in <link linkend="unstable">unstable</link>.
</para>
<para>
They must be in sync on all architectures where they have been built and
mustn't have dependencies that make them uninstallable; they also need to have
fewer release-critical bugs than the versions currently in unstable.  This way,
we hope that "testing" is always close to being a release candidate.
</para>
<para>
More information about the status of "testing" in general and the individual
packages is available at <ulink url="https://www.debian.org/devel/testing"/>.
</para>
<section id="frozen"><title>What about "testing"? How is it "frozen"?</title>
<para>
When the "testing" distribution is mature enough, the release manager starts
"freezing" it.  The normal propagation delays are increased to ensure that as
few new bugs as possible from "unstable" enter "testing".
</para>
<para>
After a while, the "testing" distribution becomes truly "frozen".  This means
that all new packages that are to propagate to the "testing" are held back,
unless they include release-critical bug fixes.  The "testing" distribution can
also remain in such a deep freeze during the so-called "test cycles", when the
release is imminent.
</para>
<para>
When a "testing" release becomes "frozen", "unstable" tends to partially freeze
as well.  This is because developers are reluctant to upload radically new
software to unstable, in case the frozen software in testing needs minor
updates and to fix release critical bugs which keep testing from becoming
"stable".
</para>
<para>
We keep a record of bugs in the "testing" distribution that can hold off a
package from being released, or bugs that can hold back the whole release.  For
details, please see <ulink
url="https://www.debian.org/releases/testing/">current testing release
information</ulink>.
</para>
<para>
Once that bug count lowers to maximum acceptable values, the frozen "testing"
distribution is declared "stable" and released with a version number.
</para>
<para>
The most important bug count is the "Release Critical" bug count, which can be
followed in the <ulink
url="https://bugs.debian.org/release-critical/">Release-critical bug status
page</ulink>.  A common release goal is <ulink
url="https://wiki.debian.org/ReleaseGoals/NoRCBugs">NoRCBugs</ulink> which means
that the distribution should not have any bugs of severity critical, grave or
serious.  The full list of issues considered critical can be found in the
<ulink url="https://release.debian.org/testing/rc_policy.txt">RC policy
document</ulink>.
</para>
<para>
With each new release, the previous "stable" distribution becomes obsolete and
moves to the archive.  For more information please see <ulink
url="https://www.debian.org/distrib/archive">Debian archive</ulink>.
</para>
</section>

</section>

<section id="unstable"><title>What does the unstable distribution contain?</title>
<para>
The "unstable" directory contains a snapshot of the current development system.
Users are welcome to use and test these packages, but are warned about their
state of readiness.  The advantage of using the unstable distribution is that
you are always up-to-date with the latest in GNU/Linux software industry, but
if it breaks: you get to keep both parts :-)
</para>
<para>
There are also main, contrib and non-free subdirectories in "unstable",
separated on the same criteria as in "stable".
</para>
</section>

<section id="dirtree"><title>What are all those directories at the Debian archives?</title>
<para>
The software that has been packaged for &debian; is available in one of
several directory trees on each Debian mirror site.
</para>
<para>
The <literal>dists</literal> directory is short for "distributions", and it is
the canonical way to access the currently available Debian releases (and
pre-releases).
</para>
<para>
The <literal>pool</literal> directory contains the actual packages, see <xref
linkend="pools"/>.
</para>
<para>
There are the following supplementary directories:
</para>
<variablelist>
<varlistentry>
<term><emphasis>/tools/</emphasis>:</term>
<listitem>
<para>
DOS utilities for creating boot disks, partitioning your disk drive,
compressing/decompressing files, and booting Linux.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/doc/</emphasis>:</term>
<listitem>
<para>
The basic Debian documentation, such as this FAQ, the bug reporting system
instructions, etc.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/indices/</emphasis>:</term>
<listitem>
<para>
various indices of the site (the Maintainers file and the override files).
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/project/</emphasis>:</term>
<listitem>
<para>
mostly developer-only materials and some miscellaneous files.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="archsections"><title>What are all those directories inside <literal>dists/stable/main</literal>?</title>
<para>
Within each of the major directory trees<footnote><para>
<literal>dists/stable/main</literal>, <literal>dists/stable/contrib</literal>,
<literal>dists/stable/non-free</literal>, and
<literal>dists/unstable/main/</literal>, etc.  </para> </footnote>, there are
three sets of subdirectories containing index files.
</para>
<para>
There's one set of
<literal>binary-<replaceable>something</replaceable></literal> subdirectories
which contain index files for binary packages of each available computer
architecture, for example <literal>binary-i386</literal> for packages which
execute on Intel x86 PC machines or <literal>binary-sparc</literal> for
packages which execute on Sun SPARCStations.
</para>
<para>
The complete list of available architectures for each release is available at
<ulink url="&url-debian-releases;">the release's web page</ulink>.
For the current release, please see <xref linkend="arches"/>.
</para>
<para>
The index files in binary-* are called Packages(.gz, .bz2) and they include a
summary of each binary package that is included in that distribution.  The
actual binary packages reside in the top level <link
linkend="pools"><literal>pool</literal> directory</link>.
</para>
<para>
Furthermore, there's a subdirectory called source/ which contains index files
for source packages included in the distribution.  The index file is called
Sources(.gz, .bz2).
</para>
<para>
Last but not least, there's a set of subdirectories meant for the installation
system index files, they are at
<literal>debian-installer/binary-<replaceable>architecture</replaceable></literal>.
</para>
</section>

<section id="source"><title>Where is the source code?</title>
<para>
Source code is included for everything in the Debian system.  Moreover, the
license terms of most programs in the system <emphasis>require</emphasis> that
source code be distributed along with the programs, or that an offer to provide
the source code accompany the programs.
</para>
<para>
The source code is distributed in the <literal>pool</literal> directory (see
<xref linkend="pools"/>) together with all the architecture-specific binary
directories.  To retrieve the source code without having to be familiar with
the structure of the archive, try a command like
<literal>apt-get source mypackagename</literal>.
</para>
<para>
Due to restrictions in their licenses, source code may or may not be available
for packages in the "contrib" and "non-free" areas, which are not formally part
of the Debian system.  In some cases only sourceless "binary blobs" can be
distributed (see for instance <literal>firmware-misc-nonfree</literal>); in
other cases the license prohibits the distribution of prebuilt binaries, but
does allow packages of source code which users can compile locally (see
<literal>broadcom-sta-dkms</literal>).
</para>
</section>

<section id="pools"><title>What's in the <literal>pool</literal> directory?</title>
<para>
Packages are kept in a large "pool", structured according to the name of the
source package.  To make this manageable, the pool is subdivided by section
("main", "contrib" and "non-free") and by the first letter of the source
package name.  These directories contain several files: the binary packages for
each architecture, and the source packages from which the binary packages were
generated.
</para>
<para>
You can find out where each package is placed by executing a command like
<literal>apt-cache showsrc mypackagename</literal> and looking at the
"Directory:" line.  For example, the <literal>apache</literal> packages are
stored in <literal>pool/main/a/apache/</literal>.
</para>
<para>
Additionally, since there are so many <literal>lib*</literal> packages, these
are treated specially: for instance, libpaper packages are stored in
<literal>pool/main/libp/libpaper/</literal>.
</para>
<para>
<footnote><para> Historically, packages were kept in the subdirectory of
<literal>dists</literal> corresponding to which distribution contained them.
This turned out to cause various problems, such as large bandwidth consumption
on mirrors when major changes were made.  This was fixed with the introduction
of the package pool.  </para> <para> The <literal>dists</literal> directories
are still used for the index files used by programs like
<literal>apt</literal>.  </para> </footnote>
</para>
</section>

<section id="incoming"><title>What is "incoming"?</title>
<para>
After a developer uploads a package, it stays for a short while in the
"incoming" directory before it is checked that it's genuine and allowed into
the archive.
</para>
<para>
Usually nobody should install things from this place.  However, in some rare
cases of emergency, the incoming directory is available at <ulink url="https://incoming.debian.org/"/>.  You can
manually fetch packages, check the GPG signature and MD5sums in the .changes
and .dsc files, and then install them.
</para>
</section>

<section id="ownrepository"><title>How do I set up my own apt-able repository?</title>
<para>
If you have built some private Debian packages which you'd like to install
using the standard Debian package management tools, you can set up your own
apt-able package archive.  This is also useful if you'd like to share your
Debian packages while these are not distributed by the Debian project.
Instructions on how to do this are given on the <ulink
url="https://wiki.debian.org/HowToSetupADebianRepository">Debian Wiki</ulink>.
</para>
</section>

</chapter>
