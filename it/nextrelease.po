# Translation of debian-faq into Italian.
#
# Copyright © Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Translators:
# Beatrice Torracca <beatricet@libero.it>, 2012-2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-06 14:47+0200\n"
"PO-Revision-Date: 2019-03-09 12:44+0200\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: Content of: <chapter><title>
#: en/nextrelease.dbk:8
msgid "Changes expected in the next major release of Debian"
msgstr "Cambiamenti attesi nel prossimo rilascio principale di Debian"

#. type: Content of: <chapter><para>
#: en/nextrelease.dbk:10
msgid ""
"With each new release, the Debian project tries to focus on a set of "
"topics.  These are known as \"Release Goals\" and they are all described in "
"<ulink url=\"https://wiki.debian.org/ReleaseGoals/\"/>.  Please note that "
"the following sections might not be fully up-to-date, please refer to the "
"Wiki for more information and the up-to-date status of these goals."
msgstr ""
"Con ogni nuovo rilascio il progetto Debian cerca di concentrarsi su un "
"insieme di argomenti. Questi sono noti come \"Obiettivi di rilascio\" e sono "
"tutti descritti in <ulink url=\"https://wiki.debian.org/ReleaseGoals/\"/>. "
"Notare che le sezioni seguenti potrebbero non essere completamente "
"aggiornate; fare riferimento al Wiki per maggiori informazioni e per lo "
"stato aggiornato di questi obiettivi."

#. type: Content of: <chapter><section><title>
#: en/nextrelease.dbk:16
msgid "Hardening the system"
msgstr "Rafforzare il sistema"

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:18
msgid ""
"It is a goal for the Debian project to ensure that any system installed is "
"hardened and secure against attacks.  There are several ways to achieve "
"this, which include:"
msgstr ""
"È un obiettivo del progetto Debian assicurare che qualsiasi sistema "
"installato sia rafforzato e sicuro contro gli attacchi. Ci sono diversi modi "
"per ottenerlo che includono:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:25
msgid ""
"Improve programs' security by compiling them with <ulink url=\"https://wiki."
"debian.org/ReleaseGoals/SecurityHardeningBuildFlags\">Security Hardening "
"Build Flags</ulink> in order to enable various protections against known "
"security issues,"
msgstr ""
"Migliorare la sicurezza dei programmi compilandoli con <ulink url=\"https://"
"wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags\">Security "
"Hardening Build Flags</ulink> (opzioni di compilazione di rafforzamento "
"della sicurezza) allo scopo di abilitare varie protezioni contro problemi di "
"sicurezza noti."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:33
msgid ""
"Improve the default system configuration to make it less vulnerable to "
"attacks (both local or remote),"
msgstr ""
"Migliorare la configurazione predefinita di sistema per renderlo meno "
"vulnerabile ad attacchi (sia in locale sia da remoto)."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:39
msgid "Enable security features delivered by new versions of the kernel."
msgstr ""
"Abilitare funzionalità di sicurezza fornite da versioni nuove del kernel."

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:44
msgid ""
"All of these are done in an ongoing basis.  For the first item, a set of "
"security hardening build flags that try to prevent known attacks such as "
"stack smashing, predictable locations of values in memory, etc.  is used.  "
"The target is to cover at least all packages that are part of the basic "
"installation as well as packages that had to be updated through a Security "
"Advisory since 2006.  As of this writing, around 400 packages have been "
"modified since this effort was first started.  All the issues are <ulink "
"url=\"https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=goal-hardening;"
"users=hardening-discuss@lists.alioth.debian.org\">tracked in the BTS</ulink>."
msgstr ""
"Tutto ciò viene fatto costantemente. Per la prima voce viene usato un "
"insieme di opzioni di compilazione per il rafforzamento della sicurezza che "
"cerca di prevenire attacchi noti, come «stack smashing», posizioni "
"prevedibili di valori in memoria, ecc. L'obiettivo è di coprire almeno tutti "
"i pacchetti che fanno parte dell'installazione base, oltre ai pacchetti che "
"è stato necessario aggiornare attraverso un Avvertimento di sicurezza a "
"partire dal 2006. Al momento della stesura di questo documento, circa 400 "
"pacchetti sono stati modificati da quando è stato iniziato questo sforzo. "
"Tutti i problemi sono <ulink url=\"https://bugs.debian.org/cgi-bin/pkgreport."
"cgi?tag=goal-hardening;users=hardening-discuss@lists.alioth.debian."
"org\">tracciati nel BTS</ulink>."

#. type: Content of: <chapter><section><title>
#: en/nextrelease.dbk:56
msgid "Extended support for non-English users"
msgstr "Supporto esteso per gli utenti non-inglesi"

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:58
msgid ""
"Debian already has very good support for non-English users, see <xref "
"linkend=\"nonenglish\"/>."
msgstr ""
"Debian ha già un ottimo supporto per utenti non-inglesi, si veda <xref "
"linkend=\"nonenglish\"/>."

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:62
msgid ""
"We hope to find people who will provide support for even more languages, and "
"translate programs and documents.  Many programs and Debian-specific "
"documents already support internationalization, so we need message catalogs "
"translators.  However, still some programs remain to be properly "
"internationalized."
msgstr ""
"Ci si augura di trovare persone che forniscano supporto per ancora più "
"lingue e che traducano programmi e documenti. Molti programmi e documenti "
"specifici di Debian supportano già l'internazionalizzazione, così abbiamo "
"bisogno di traduttori dei cataloghi dei messaggi. Tuttavia rimangono ancora "
"programmi che devono essere correttamente internazionalizzati."

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:68
msgid ""
"The GNU Translation Project <ulink url=\"ftp://ftp.gnu.org/pub/gnu/ABOUT-"
"NLS\"/> works on internationalizing the GNU programs and different projects, "
"such as the Desktop environments GNOME or KDE have their own translation "
"teams.  The goal of Debian is not to replace or to repeat the work done by "
"these projects, indeed, Debian benefits from the work done by translators in "
"these projects.  However, there are still many programs which are not in the "
"scope of those projects and which are translated within Debian."
msgstr ""
"Il GNU Translation Project <ulink url=\"ftp://ftp.gnu.org/pub/gnu/ABOUT-"
"NLS\"/> lavora sull'internazionalizzazione dei programmi GNU e diversi "
"progetti, come gli ambienti desktop GNOME e KDE, hanno dei propri gruppi di "
"traduzione. L'obiettivo di Debian non è quello di sostituire o ripetere il "
"lavoro fatto da questi progetti; in verità Debian trae beneficio dal lavoro "
"fatto dai traduttori in questi progetti. Tuttavia ci sono ancora molti "
"programmi che non rientrano nell'ambito di questi progetti e che sono "
"tradotti all'interno di Debian."

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:77
msgid "Previous Debian releases have focused in topics such as:"
msgstr "I precedenti rilasci di Debian si sono concentrati su aspetti come:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:82
msgid ""
"I18n support in all debconf-using packages: Packages using the Debian "
"configuration management must allow for translation of all messages "
"displayed to the user during package configuration."
msgstr ""
"Supporto i18n in tutti i pacchetti che usano debconf: i pacchetti che usano "
"la gestione Debian della configurazione devono permettere la traduzione di "
"tutti i messaggi presentati all'utente durante la configurazione del "
"pacchetto."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:89
msgid ""
"I18n support for package descriptions: Update package management frontends "
"to use the translated descriptions of packages."
msgstr ""
"Supporto i18n per le descrizioni dei pacchetti: aggiornare le interfacce "
"alla gestione dei pacchetti in modo che usino le descrizioni tradotte dei "
"pacchetti."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:95
msgid ""
"UTF-8 debian/changelog and debian/control.  This way, e.g. names of people "
"from asian countries can get typeset the right way in changelogs."
msgstr ""
"debian/changelog e debian/control UTF-8. In questo modo è possibile, per "
"esempio, stampare correttamente nei changelog i nomi delle persone "
"provenienti dai paesi asiatici."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/nextrelease.dbk:101
msgid ""
"I18n support in the Debian Installer including full support for some "
"languages that require the use of the graphical interface."
msgstr ""
"Supporto i18n nell'installatore Debian incluso il pieno supporto per alcune "
"lingue che richiedono l'uso dell'interfaccia grafica."

#. type: Content of: <chapter><section><title>
#: en/nextrelease.dbk:108
msgid "Improvements in the Debian Installer"
msgstr "Miglioramenti nell'installatore Debian"

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:110
msgid ""
"Lots of work has been done on the Debian Installer, resulting in major "
"improvements.  We'll mention just two of them here."
msgstr ""
"È stato fatto molto lavoro sull'installatore Debian che ha portato a grandi "
"miglioramenti. Ne citeremo qui solo due."

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:114
msgid ""
"Starting the installer from Microsoft Windows: It is now possible to start "
"the installer directly from Microsoft Windows without the need to change "
"BIOS settings.  Upon insertion of a CD-ROM, DVD-ROM or USB stick, an autorun "
"program will be started, offering a step-by-step process to start the Debian "
"Installer."
msgstr ""
"Avvio dell'installatore da Microsoft Windows: è ora possibile avviare "
"l'installatore direttamente da Microsoft Windows senza bisogno di modificare "
"le impostazioni del BIOS. All'inserimento di un CD-ROM, DVD-ROM o pennina "
"USB, un programma eseguito automaticamente verrà avviato, offrendo una "
"procedura passo-passo per avviare l'installatore Debian."

#. type: Content of: <chapter><section><title>
#: en/nextrelease.dbk:121
msgid "More architectures"
msgstr "Più architetture"

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:123
msgid ""
"Complete Debian system on other architectures.  Notice that even though some "
"architectures are dropped for a given release, there still might be a way to "
"install and upgrade using the latest <literal>sid</literal>."
msgstr ""
"Un sistema Debian completo su altre architetture. Notare che benché alcune "
"architetture vengano dismesse per un dato rilascio, ci potrebbe comunque "
"essere un modo per installarle e aggiornarle usando la più recente "
"<literal>sid</literal>."

#. type: Content of: <chapter><section><title>
#: en/nextrelease.dbk:129
msgid "More kernels"
msgstr "Più kernel"

#. type: Content of: <chapter><section><para>
#: en/nextrelease.dbk:131
msgid ""
"In addition to Debian GNU/Hurd, Debian is being ported also to BSD kernels, "
"namely to <ulink url=\"https://www.debian.org/ports/kfreebsd-gnu/\">FreeBSD</"
"ulink>.  This port runs on both AMD64 (\"kfreebsd-amd64\") and traditional "
"Intel (\"kfreebsd-i386\")."
msgstr ""
"In aggiunta a Debian GNU/Hurd, Debian sta facendo il port anche per i kernel "
"BSD, in particolare per <ulink url=\"https://www.debian.org/ports/kfreebsd-"
"gnu/\">FreeBSD</ulink>. Questo port gira sia su macchine AMD64 (\"kfreebsd-"
"amd64\") sia su Intel tradizionali (\"kfreebsd-i386\")."
